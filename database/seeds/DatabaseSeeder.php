<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->TruncateTable([
            'users',
            'departments',
            'services',
            'tickets',
            'messages',
            'files',
            'subjects'
        ]);

        $this->call(DepartmentSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(ServiceSeeder::class);
        $this->call(TicketSeeder::class);
        $this->call(MessageSeeder::class);
        $this->call(FileSeeder::class);
        $this->call(SubjectSeeder::class);
    }

    protected function TruncateTable(array $tables){
        DB::statement('SET FOREIGN_KEY_CHECKS = 0;');  
        // se desactiva la llave foranea

        foreach ($tables as $table) {
            DB::table($table)->truncate();
            // Vacia la tabla
        }
        
        DB::statement('SET FOREIGN_KEY_CHECKS = 1;');  
        // se activa la llave foranea
    }
}
