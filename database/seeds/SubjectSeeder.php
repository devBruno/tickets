<?php
use App\Subject;
use App\Service;
use Illuminate\Database\Seeder;

class SubjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $serviceId = Service::where('name', 'Servicio de correo electrónico')->value('id');
        
        Subject::create([
    		'name' => 'Respaldo de información',
    		'service_id' => $serviceId,
    	]);
    }
}
