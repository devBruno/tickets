<?php
use App\User;
use App\Department;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $department_id = Department::where('name', 'Dirección')->value('id');

        factory(User::class)->create([
            'name' => 'AMMMEC S.A DE C.V',
            'email' => 'ammmec@ammmec.com',
            'password' => bcrypt('Admin.DSI'),
            'employee_number' => '0000',
            'department_id' => $department_id,
            'is_admin' => true,
        ]);

        factory(User::class)->create([
            'name' => 'Felipe de Jesús Esquivel Hernández',
            'email' => 'felipe_esquivel@ammmec.com',
            'password' => bcrypt('Amm-DDG.1'),
            'employee_number' => '001-010512',
            'department_id' => $department_id,
            'is_admin' => false,
        ]);

        factory(User::class)->create([
            'name' => 'Estela Haydde Hernández Salas',
            'email' => 'h_hernandez@ammmec.com',
            'password' => bcrypt('Amm-DDG.2'),
            'employee_number' => '008-280111',
            'department_id' => $department_id,
            'is_admin' => false,
        ]);

        $department_id = Department::where('name', 'Sistemas')->value('id');

        factory(User::class)->create([
            'name' => 'Alejandro Castro Saucedo',
            'email' => 'sistemas@ammmec.com',
            'password' => bcrypt('Amm-DSI.3'),
            'employee_number' => '157-170918',
            'department_id' => $department_id,
            'is_admin' => true,
        ]);

        factory(User::class)->create([
            'name' => 'Bruno Padilla Guerrero',
            'email' => 'sistema_erp@ammmec.com',
            'password' => bcrypt('Amm-DSI.4'),
            'employee_number' => '158-170918',
            'department_id' => $department_id,
            'is_admin' => true,
        ]);

        $department_id = Department::where('name', 'Finanzas')->value('id');

        factory(User::class)->create([
            'name' => 'Heriberto Sánchez Triana',
            'email' => 'finanzas_fresnillo@ammmec.com',
            'password' => bcrypt('Amm-DFN.5'),
            'employee_number' => '121-020216',
            'department_id' => $department_id,
            'is_admin' => false,
        ]);

        factory(User::class)->create([
            'name' => 'Heriberto Sánchez Triana',
            'email' => 'heriberto_sanchez@ammmec.com',
            'password' => bcrypt('Amm-DFN.6'),
            'employee_number' => '121-020216',
            'department_id' => $department_id,
            'is_admin' => false,
        ]);

        factory(User::class)->create([
            'name' => 'Jessica E. Guardado Araiza',
            'email' => 'contabilidad_fresnillo@ammmec.com',
            'password' => bcrypt('Amm-DFN.7'),
            'employee_number' => '136-300117',
            'department_id' => $department_id,
            'is_admin' => false,
        ]);

        $department_id = Department::where('name', 'Compras')->value('id');

        factory(User::class)->create([
            'name' => 'Edgar I Mauricio R.',
            'email' => 'compras_fresnillo@ammmec.com',
            'password' => bcrypt('Amm-DSM.8'),
            'employee_number' => '0000',
            'department_id' => $department_id,
            'is_admin' => false,
        ]);

        $department_id = Department::where('name', 'Conservación')->value('id');

        factory(User::class)->create([
            'name' => 'Oscar D. Murillo Ramírez',
            'email' => 'conservacion_fresnillo@ammmec.com',
            'password' => bcrypt('Amm-DSM.9'),
            'employee_number' => '0000',
            'department_id' => $department_id,
            'is_admin' => false,
        ]);

        $department_id = Department::where('name', 'Suministros')->value('id');

        factory(User::class)->create([
            'name' => 'Beatriz Rico Perez',
            'email' => 'suministros_fresnillo@ammmec.com',
            'password' => bcrypt('Amm-DSM.10'),
            'employee_number' => '147-150318',
            'department_id' => $department_id,
            'is_admin' => false,
        ]);

        $department_id = Department::where('name', 'Ventas')->value('id');

        factory(User::class)->create([
            'name' => 'Yunuen C. Tafolla Briones',
            'email' => 'yunuen@ammmec.com',
            'password' => bcrypt('Amm-DVT.11'),
            'employee_number' => '127-250816',
            'department_id' => $department_id,
            'is_admin' => false,
        ]);

        factory(User::class)->create([
            'name' => 'Yunuen C. Tafolla Briones',
            'email' => 'ventas_fresnillo@ammmec.com',
            'password' => bcrypt('Amm-DVT.12'),
            'employee_number' => '127-250816',
            'department_id' => $department_id,
            'is_admin' => false,
        ]);

        factory(User::class)->create([
            'name' => 'Felipe Antonio Lomelí Fernández',
            'email' => 'felipe_lomeli@ammmec.com',
            'password' => bcrypt('Amm-DVT.13'),
            'employee_number' => '138-240717',
            'department_id' => $department_id,
            'is_admin' => false,
        ]);

        $department_id = Department::where('name', 'Operaciones')->value('id');

        factory(User::class)->create([
            'name' => 'Felipe Antonio Esquivel Rico',
            'email' => 'fesquivel@ammmec.com',
            'password' => bcrypt('Amm-DOP.14'),
            'employee_number' => '025-040411',
            'department_id' => $department_id,
            'is_admin' => false,
        ]);

        factory(User::class)->create([
            'name' => 'Victor O. Arroyo Ibañez',
            'email' => 'centrodeservicio@ammmec.com',
            'password' => bcrypt('Amm-DOP.15'),
            'employee_number' => '149-180618',
            'department_id' => $department_id,
            'is_admin' => false,
        ]);

        factory(User::class)->create([
            'name' => 'José Carlos Alvarado Caneles',
            'email' => 'servicioencampo@ammmec.com',
            'password' => bcrypt('Amm-DOP.16'),
            'employee_number' => '144-261017',
            'department_id' => $department_id,
            'is_admin' => false,
        ]);

        factory(User::class)->create([
            'name' => 'Margarita Martínez Hinostroza',
            'email' => 'planeacion_fresnillo@ammmec.com',
            'password' => bcrypt('Amm-DOP.17'),
            'employee_number' => '140-030817',
            'department_id' => $department_id,
            'is_admin' => false,
        ]);

        // factory(User::class)->create([
        //     'name' => 'Juan Manuel Flores Montes',
        //     'email' => 'servicioencampo2@ammmec.com',
        //     'password' => bcrypt('Amm-DOP.18'),
        //     'employee_number' => '0000',
        //     'department_id' => $department_id,
        //     'is_admin' => false,
        // ]);

        factory(User::class)->create([
            'name' => 'Oscar Martínez Villagrana',
            'email' => 'oscar_martinez@ammmec.com',
            'password' => bcrypt('Amm-DOP.19'),
            'employee_number' => '0000',
            'department_id' => $department_id,
            'is_admin' => false,
        ]);

        $department_id = Department::where('name', 'Recursos Humanos')->value('id');

        factory(User::class)->create([
            'name' => 'Mayra G. Ortega Saldaña',
            'email' => 'mayra_ortega@ammmec.com',
            'password' => bcrypt('Amm-DRH.20'),
            'employee_number' => '103-231115',
            'department_id' => $department_id,
            'is_admin' => false,
        ]);

        factory(User::class)->create([
            'name' => 'Mayra G. Ortega Saldaña',
            'email' => 'recursos_humanos@ammmec.com',
            'password' => bcrypt('Amm-DRH.21'),
            'employee_number' => '103-231115',
            'department_id' => $department_id,
            'is_admin' => false,
        ]);

        $department_id = Department::where('name', 'Seguridad')->value('id');

        factory(User::class)->create([
            'name' => 'Guillermo Murillo Ramírez',
            'email' => 'seguridad_fresnillo@ammmec.com',
            'password' => bcrypt('Amm-DSE.22'),
            'employee_number' => '083-120813',
            'department_id' => $department_id,
            'is_admin' => false,
        ]);

        $department_id = Department::where('name', 'Normatividad')->value('id');

         factory(User::class)->create([
            'name' => 'Guillermo Murillo Ramírez',
            'email' => 'guillermo_murillo@ammmec.com',
            'password' => bcrypt('Amm-DNR.23'),
            'employee_number' => '083-120813',
            'department_id' => $department_id,
            'is_admin' => false,
        ]);

        $department_id = Department::where('name', 'Mercadotecnia')->value('id');

        factory(User::class)->create([
            'name' => 'Stephanie C. Qualls Portillo',
            'email' => 'mercadotecnia_fresnillo@ammmec.com',
            'password' => bcrypt('Amm-DMK.24'),
            'employee_number' => '148-310518',
            'department_id' => $department_id,
            'is_admin' => false,
        ]);

        $department_id = Department::where('name', 'Almacén')->value('id');
       
        factory(User::class)->create([
            'name' => 'Roque Guardado Carrillo',
            'email' => 'almacen_fresnillo@ammmec.com',
            'password' => bcrypt('Amm-DSM.25'),
            'employee_number' => '038-160611',
            'department_id' => $department_id,
            'is_admin' => false,
        ]);

         $department_id = Department::where('name', 'Operaciones')->value('id');

        factory(User::class)->create([
            'name' => 'Ricardo Alvarado',
            'email' => 'ricardo_alvarado@ammmec.com',
            'password' => bcrypt('Amm-DOP.26'),
            'employee_number' => '000-000-00',
            'department_id' => $department_id,
            'is_admin' => false,
        ]);
    }
}
