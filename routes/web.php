<?php

// Auth::routes();

Route::get('/', 'Auth\LoginController@showLoginForm');

Route::post('/login', 'Auth\LoginController@login')
->name('login');

Route::post('/logout', 'Auth\LoginController@logout')
->name('logout');

// Registration Routes...

Route::get('registro', 'Auth\RegisterController@showRegistrationForm')->name('register');

Route::post('registro', 'Auth\RegisterController@register');

// Password Reset Routes...

// Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
// Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
// Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
// Route::post('password/reset', 'Auth\ResetPasswordController@reset');

// Route::get('/home', 'HomeController@index')->name('home');

Route::get('/tickets', 'TicketController@index')
->name('tickets');

Route::get('/tickets/admin', 'TicketController@indexAdmin')
->name('tickets.admin');

Route::get('/tickets/nuevo', 'TicketController@create')
->name('tickets.create');

Route::get('/tickets/nuevo/admin', 'TicketController@create_admin')
->name('tickets.create_admin');

Route::get('/tickets/{ticket}/', 'TicketController@show')
->name('tickets.show');

Route::get('/tickets/{ticket}/admin', 'TicketController@show_admin')
->name('tickets.show_admin');

Route::get('tickets/download/{file}' , 'TicketController@download')
->name('tickets.downloadFile');


Route::get('/usuarios', 'UserController@index')
->name('users');

Route::get('/usuarios/{user}', 'UserController@show')
->name('users.show');

Route::get('/usuarios/filter/{status}', 'UserController@getUsers')
->name('users.getUsers');

Route::post('/usuarios/updateType', 'UserController@updateType')
->name('user.updateType');

Route::get('/usuarios/getUser/{user}', 'UserController@getUser')
->name('users.getUser');

Route::get('/servicios', 'ServiceController@index')
->name('services');

Route::post('/servicios', 'ServiceController@store')
->name('services.store');

Route::get('/servicios/{service}', 'ServiceController@show')
->name('services.show');

Route::get('/servicios/filter/{status}', 'ServiceController@getServices')
->name('services.getServices');

Route::post('/servicios/updateStatus', 'ServiceController@updateStatus')
->name('service.updateStatus');

Route::get('/servicios/listSubjects/{service_id}', 'ServiceController@listSubjects')
->name('services.listSubjects');

Route::get('/servicios/listarUsuarios/{service}', 'ServiceController@listUsers')
->name('services.listUsers');

Route::get('/servicios/obtenerUsuarios/{service}', 'ServiceController@getUsers')
->name('services.getUsers');

Route::post('/servicios/agregarUsuario/', 'ServiceController@attachUser')
->name('services.attachUser');

Route::delete('/servicios/eliminarUsuario/', 'ServiceController@detachUser')
->name('services.detachUser');

Route::post('/servicios/agregarAsunto/', 'ServiceController@addSubject')
->name('services.addSubject');

Route::delete('/servicios/{subject}', 'ServiceController@destroySubject')
->name('services.destroySubject');

Route::put('servicios/editarNombreServicio/{service}','ServiceController@updateNameService')
->name('services.updateNameService');

Route::put('servicios/editarDescripcionServicio/{service}','ServiceController@updateDescriptionService')
->name('services.updateDescriptionService');



Route::get("/perfil", 'UserController@profile')
->name('profile');

Route::get('/tickets/listServices/{departmentId}', 'TicketController@listServices')
->name('tickets.listServices');

Route::get('/tickets/nuevo/{ticket}', 'TicketController@created')
->name('tickets.created');

Route::get('/tickets/nuevo_admin/{ticket}', 'TicketController@created_admin')
->name('tickets.created_admin');

Route::get('/tickets/filter/{status}', 'TicketController@getTickets')
->name('tickets.getTickets');

Route::get('/tickets/filter/{status}/admin', 'TicketController@getTickets_admin')
->name('tickets.getTickets_admin');

Route::put('/perfil/updateProfile', 'UserController@updateProfile');

Route::put('/perfil/updatePassword', 'UserController@updatePassword');

Route::post('/perfil/updateImage', 'UserController@updateImage');

Route::post('/tickets', 'TicketController@store');

Route::post('/tickets/{ticket}', 'TicketController@newMessage');

Route::get('/tickets/{ticket}/cerrar', 'TicketController@closeTicket');


Route::get('/notificaciones/{notification_id}/{route_name}/{id}', 'NotificationsController@read')->name('notifications.read');

Route::get('/notificaciones', 'NotificationsController@index')->name('notifications');



