@extends('layout')
@section('content')

<div class="container">
	<div class="row">
		<div class="col-md-3 col-lg-3 mt-4">
			<ul class="list-group buttons">
				<li class="list-group-item bg-light">
					<h1 class="display-1 mt-2" style="font-size: 25px">Ver</h1>
				</li>
				<li class="list-group-item">
					<input id="radiobtn_1" class="radiobtn" name="filter" type="radio" value="All" tabindex="1" checked>
					<span></span>
					<label for="radiobtn_1">Todos</label>
				</li>

				<li class="list-group-item">
					<input id="radiobtn_2" class="radiobtn" name="filter" type="radio" value="Activo" tabindex="1">
					<span></span>
					<label for="radiobtn_2">Activos</label>
				</li>

				<li class="list-group-item">
					<input id="radiobtn_3" class="radiobtn" name="filter" type="radio" value="Inactivo" tabindex="1">
					<span></span>
					<label for="radiobtn_3">Inactivos</label>
				</li>
			</ul>           
		</div>
		<div role="main" class="col-md-9 col-lg-9 mt-4 px-2 {{-- toast fade show pt-2 pb-4 pr-3 pl-3 --}}">
			<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center mt-1 pb-2 mb-2">
				<h1 class="display-4 text-danger" style="font-size: 40px;">Servicios disponibles</h1>
				<div class="btn-toolbar mb-2 mb-md-0">
					<button type="button" class="btn btn-sm btn-outline-danger" data-toggle="modal" data-target="#mAddService" data-whatever="{{ auth()->user()->department->name }}" onclick="openModalService()"><i class="fas fa-plus-circle"></i> Agregar servicio</button>
				</div>
			</div>

			@if($services->isNotEmpty())
			<table class="table" id="table-services" style="width: 100%;">
				<thead>
					<tr>
						<th scope="col" width="25%">Servicio</th>
						<th scope="col">Descripción</th>
						@if(auth()->user()->department_id==2)
						<th scope="col" width=""><center>Disponible</center></th>
						@endif
					</tr>
				</thead>
				<tbody>

				</tbody>
				<tfoot>
					<tr>
						<th scope="col">Servicio</th>
						<th scope="col">Descirpción</th>
						@if(auth()->user()->department_id==2)
						<th scope="col"><center>Disponible</center></th>
						@endif
					</tr>
				</tfoot>
			</table>
			@else
			<div class="row justify-content-center">
				<div>
					<img src="{{ asset('images/system/sin_servicios.png') }}" alt="sin_servicios" class="center-block imag" style="max-width: 430px; margin-top: 80px;">
				</div>
			</div>
			@endif

		</div>
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="mAddService" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header bg-secondary text-light">
				<h5 class="modal-title" id="exampleModalLabel">Nuevo servicio</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form method="POST" action="{{ route('services.store') }}" id="form-service">
				{!! csrf_field() !!}
				<div class="modal-body">
					<div class="form-group">
						<label for="message-text" class="col-form-label">Nombre del servicio</label>
						<input type="text" class="form-control" id="service" name="service">
						<div class="invalid-feedback mb-2 mt-2 error-message-service"></div>
					</div>
					<div class="form-group">
						<label for="description" class="col-form-label">Descripción del servicio</label>
						<textarea name="description" class="form-control" id="description"></textarea>
						<div class="invalid-feedback mb-2 mt-2 error-message-description"></div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-light" data-dismiss="modal">Cancelar</button>
					<button type="submit" class="btn btn-danger">Guardar</button>
				</div>
			</form>
		</div>
	</div>
</div>


<div class="modal fade" id="mSuccess" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header bg-light text-success">
				<h5 class="display-1 mt-2" style="font-size: 20px;" id="modalLabel">Éxito</h5>
				<a href="{{ route("services") }}" role="button" class="close">
					<span aria-hidden="true">&times;</span>
				</a>
			</div>
			<div class="modal-body">
				<h1 class="display-1" style="font-size: 20px;">Se ha registrado correctamente el servicio.</h1>
			</div>
			<div class="modal-footer">
				<a href="{{ route("services") }}" role="button" class="btn btn-sm btn-secondary">Aceptar</a>
			</div>
		</div>
	</div>
</div>

<div class="modal fade bd-example-modal-xl" id="mSubjects" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-xl">
		<div class="modal-content">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="display-1 mt-2" style="font-size: 25px;" id="labService"></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close" data-toggle="tooltip" data-placement="right" title="Tooltip on right">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-4 col-lg-4">
							<ul class="list-group buttons">

								<div class="toast-header bg-light text-dark list-group-item">

									<h1 class="display-1 mt-2" style="font-size: 20px">Responsables</h1>

									<strong class="mr-auto"></strong>

									<small class="text-muted"><a href="#div-add-user" class="text-danger" data-toggle="collapse" aria-expanded="false" aria-controls="div-add-user" id="button-add-user"><i class="fas fa-user-plus"></i> Agregar</a></small>

								</div>

								<div class="text-dark list-group-item collapse" style="padding: .75rem 1.25rem; max-width: 100%" id="div-add-user">
									<form method="POST" action="{{ route('services.attachUser') }}" id="form-users">
										{!! csrf_field() !!}
										<input name="service_id" class="form-control form-control-sm service_id" hidden>
										<div class="form-group">
											<select name="user_id" id="select-users" class="form-control form-control-sm">

											</select>
										</div>
										<div>
											<button type="button" class="btn btn-secondary btn-sm" data-toggle="collapse" href="#div-add-user" aria-expanded="false" aria-controls="div-add-user">Terminar</button>
											<button type="submit" class="btn btn-danger btn-sm">Guardar</button>
										</div>
									</form>
								</div>

								<div id="list_users">

								</div>

							</ul>           
						</div>
						<div class="col-md-8 col-lg-8">
							<ul class="list-group buttons">
								<div class="toast-header bg-light text-dark list-group-item">
									<h1 class="display-1 mt-2" style="font-size: 20px">Asuntos para atender este tema</h1>s
									<strong class="mr-auto"></strong>
									<small class="text-muted"><a href="#agrega_asunto" class="text-danger" data-toggle="collapse" aria-expanded="false" aria-controls="agrega_asunto"><i class="fas fa-plus-circle" ></i> Agregar</a></small>
								</div>

								<div class="text-dark list-group-item collapse" style="padding: .75rem 1.25rem; max-width: 100%" id="agrega_asunto">
									<form method="POST" action="{{ route('services.addSubject') }}" id="form-subjects">
										{!! csrf_field() !!}
										<input name="service_id" class="form-control form-control-sm service_id" hidden>

										<div class="form-group">
											<input name="subject" id="inputSubject" class="form-control form-control-sm" placeholder="Ingresa un nuevo asunto">
										</div>
										<div>
											<button type="button" class="btn btn-secondary btn-sm" data-toggle="collapse" href="#agrega_asunto" aria-expanded="false" aria-controls="agrega_asunto">Terminar</button>
											<button type="submit" class="btn btn-danger btn-sm">Guardar</button>
										</div>

									</form>
								</div>

								<div id="list_subjects">
									<div class="toast-header text-dark list-group-item" style="    padding: .75rem 1.25rem;">
										Tengo otro tipo de problemas
										<strong class="mr-auto"></strong>
										<small class="text-muted"><a href="#" class="text-danger"><i class="fas fa-minus-circle"></i></a></small>
									</div>
								</div>

							</ul>           
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-light text-danger pl-5 pr-5" data-dismiss="modal">Terminar</button>
				</div>
			</div>
		</div>
	</div>
</div>


@endsection
@section('script')
@routes
<script type="text/javascript">
	status = "All";
	service_id = null;
	form_id = null;

	$(document).ready(function() {  
		table(status);

		$('.radiobtn').click(function() {
			status = $('input:radio[name=filter]:checked').val();
			table(status);
		});

		$('*[data-href]').on('click', function() {
			window.location = $(this).data("href");
		});

		$('.btn-submit').click(function(){
			alert('entra');
		});

		$('form').submit(function(e){
			e.preventDefault();
			var id_form = $(this).attr('id');
			var class_form = $(this).attr('class');

			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
				}
			});
			$.ajax({
				url:  $(this).attr('action'),
				method: $(this).attr('method'),
				data:   $(this).serialize(),
				success: function(data){
					if(id_form=='form-users'|| class_form=='form-user-delete'){
						renderUsers();
					}
					if(id_form=='form-subjects' || class_form=='form-subject-delete'){
						$('#inputSubject').val('');
						renderSubjects();
					}
					if(id_form=='form-service'){
						$('#mAddService').modal('toggle');
						$('#mSuccess').modal('toggle');
					}
				},
				error: function (data) {
					cleanInputsSuccess();
					for (var [campo, message] of Object.entries(data.responseJSON.errors)){
						$('.error-message-'+campo).empty();
						$('#'+campo).addClass('is-invalid');
						$('.error-message-'+campo).append('<p>'+message+'</p>');
					}
				}
			});
		});

		$('#button-add-user').click(function(event) {
			if(!$('#div-add-user').hasClass('show')){
				$.get(route('services.getUsers', 
					[service_id]), 
				function(users){
					var users = JSON.parse(users);
					$("#select-users").empty();
					var selector = document.getElementById("select-users");
					selector.options[0] = new Option("Seleccione ...","");
					for (var i in users) {
						var j = parseInt(i) + 1;
						selector.options[j] = new Option(users[i].name+" ("+users[i].email+")",users[i].id);
					}
				});
			}
		});
	});

	function cleanInputsSuccess() {
		var campos = ['service', 'description'];
		for (var i in campos){
			$('.error-message-'+campos[i]).empty();
			$('#'+campos[i]).removeClass('is-invalid');
		}
	}

	function table(status)
	{
		var table = $('#table-services').DataTable( {
			paging:   true,
			ordering: true,
			info:     true,
			filter:   true,
			processing: true,
			serverSide: true,
			destroy: true,
			scrollX: true,
			order: [[ 0, "desc" ]],
			language: {
				lengthMenu: "Mostrar _MENU_ registros por página",
				zeroRecords: "No se encontró ningún registro",
				info: "Mostrando página _PAGE_ de _PAGES_",
				infoEmpty: "No hay registros disponibles",
				infoFiltered: "(Filtrado de _MAX_ registros)",
				search: "Buscar:",
				processing: "Procesando...",
				paginate: {
					first:      "Inicio",
					last:       "Fin",
					next:       "Siguiente",
					previous:   "Anterior"
				},
			},
			ajax:  {
				url: route('services.getServices', [status])
			},
			columns: [
			{ data: 'name', name: 'name' },
			{ data: 'description', name: 'description' },
			@if(auth()->user()->department_id==2) 
			{ data: 'status', name: 'status', align: 'center' }
			@endif
			],
			search: {
				"regex": true
			}
		});
	} 

	function detalles(id, name){
		service_id = id;
		$('.service_id').val(service_id);
		$('#labService').html(name+' <a href="#" class="text-danger" data-toggle="modal" data-target="#mEditNameService"><i class="fas fa-edit fa-xs" style="font-size:13px;"></i></a>');
		renderSubjects();
		renderUsers();
	}

	function renderSubjects(){
		$.get(route('services.listSubjects', 
			{ service_id: service_id }), 
		function(subjects){
			var subjects = JSON.parse(subjects);
			$("#list_subjects").empty();
			var subj='';

			for (var i in subjects){
				subject_id=subjects[i].id;
				subj = subj + `<div class="toast-header text-dark list-group-item" style="padding: .75rem 1.25rem;"><form method="POST" action="`+route('services.destroySubject', [subject_id])+`" class="form-subject-delete">
				{!! csrf_field() !!}
				{{ method_field('DELETE') }}`+
				subjects[i].nombre+ `<strong class="mr-auto"></strong><small class="text-muted"><a href="#" class="text-danger btn-submit"><i class="fas fa-minus-circle"></i></a></small></form></div>`;
			}

			if(subj==''){
				subj = `<div class="toast-header text-dark list-group-item" style="padding: .75rem 1.25rem;"><h5 class="mb-1 text-secondary display-1"  style="font-size: 20px; ">No hay ningún asunto registrado</h5><strong class="mr-auto"></strong></div>`;
			}

			$("#list_subjects").html(subj);
			// detectForms();
		});
	}

	function renderUsers(){
		$.get(route('services.listUsers', [service_id]), 
			function(users){
				var users = JSON.parse(users);
				$("#list_users").empty();
				var subj='';

				for (var i in users){

					subj = subj + `<div class="toast-header text-dark list-group-item" style="padding: .75rem 1.25rem;">
					<form method="POST" action="{{ route('services.detachUser') }}" class="form-user-delete">
					{!! csrf_field() !!}
					{{ method_field('DELETE') }}
					<input hidden name="service_id" value="`+service_id+`">
					<input hidden name="user_id" value="`+users[i].id+`">
					<img src="{{ asset('/') }}/`+users[i].picture+`" class="rounded-circle" alt="Usuario" style="margin-right: 8px" />
					<label style="padding-top:7px;">`+users[i].name+`</label>
					<strong class="mr-auto"></strong>
					<small class="text-muted"><button class="btn btn-submit text-danger btn-link btn-sm"><i class="fas fa-minus-circle"></i></button></small>
					</form>
					</div>`;
				}

				if(subj==''){
					subj = `<div class="toast-header text-dark list-group-item" style="padding: .75rem 1.25rem;"><h5 class="mb-1 text-secondary display-1"  style="font-size: 20px; ">No hay ningún responsable asignado</h5><strong class="mr-auto"></strong></div>`;
				}

				$("#list_users").html(subj);
				// detectForms();

			});
	}

	function openModalService() {
		$('.error-message-service').empty();
		$('#service').removeClass('is-invalid');
	}

	function changeStatus(id, service_status) {
		$.ajax({ 
			method: 'POST',
			url:  '{{ url('/servicios/updateStatus') }}', 
			data: {
				"_token": "{{ csrf_token() }}",
				"id": id,
				"status": service_status
			}, 
			success: function(message){ 
				table(status);
				$('.font-message').html('Se ha actualizado correctamente el estado del servicio');
				$('#div-success').show();
				setTimeout(function() {
					$("#div-success").fadeOut(3500);
				},5000);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				$('.font-message').html('Ha ocurrido un error al actualizar el estado del servicio');
				$('#div-error').show();
				setTimeout(function() {
					$("#div-error").fadeOut(3500);
				},5000);
			}
		});
	}
</script>
@endsection