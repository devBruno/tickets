@extends('layout')
@section('content')


<div class="container pt-4">
	<div class="card">
		<div class="card-header">
			<h1 class="display-1 mt-2" style="font-size: 20px">Datos del servicio</h1>

		</div>
		<div class="card-body">
			<h1 class="display-4" style="font-size: 30px;">{{ $service->name }} <a href="#" class="text-danger" data-toggle="modal" data-target="#mEditNameService" id="btn-edit-name"><i class="fas fa-edit fa-xs" style="font-size:13px;"></i></a></h1>
			<p style="font-size: 15px;">{{ $service->description }} <a href="#" class="text-danger" data-toggle="modal" data-target="#mEditDescriptionService" id="btn-edit-description"><i class="fas fa-edit fa-xs" style="font-size:13px;"></i></a></p>
		</div>
	</div>

	
	<div class="row mt-4">
		<div class="col-md-5 col-lg-5">
			<ul class="list-group buttons">

				<div class="toast-header bg-light text-dark list-group-item">

					<h1 class="display-1 mt-2" style="font-size: 20px">Responsables</h1>

					<strong class="mr-auto"></strong>

					<small class="text-muted"><a href="#div-add-user" class="text-danger" data-toggle="collapse" aria-expanded="false" aria-controls="div-add-user" id="button-add-user"><i class="fas fa-user-plus"></i> Agregar</a></small>

				</div>

				<div class="text-dark list-group-item collapse" style="padding: .75rem 1.25rem; max-width: 100%" id="div-add-user">
					<form method="POST" action="{{ route('services.attachUser') }}" id="form-users">
						{!! csrf_field() !!}
						<input name="service_id" class="form-control form-control-sm" hidden value="{{ $service->id }}">
						<div class="form-group">
							<select name="user_id" id="select-users" class="form-control form-control-sm">+
								<option value="">Seleccione...</option>
								@foreach(auth()->user()->department->users as $user):
								<option value="{{ $user->id }}">{{ $user->name }} - {{ $user->email }}</option>
								@endforeach
							</select>
						</div>
						<div>
							<button type="button" class="btn btn-secondary btn-sm" data-toggle="collapse" href="#div-add-user" aria-expanded="false" aria-controls="div-add-user">Cancelar</button>
							<button type="submit" class="btn btn-danger btn-sm">Guardar</button>
						</div>
					</form>
				</div>

				@if($service->users->isNotEmpty())
				@foreach($service->users as $user)
				<div class="text-dark list-group-item" style="padding: .75rem 1.25rem;">
					<form method="POST" action="{{ route('services.detachUser') }}" class="form-user-delete">
						{!! csrf_field() !!}
						{{ method_field('DELETE') }}
						<input hidden name="service_id" value="{{ $service->id }}">
						<input hidden name="user_id" value="{{ $user->id }}">
						<img src="{{ asset($user->picture) }}" class="rounded-circle" alt="Usuario" style="margin-right: 8px" />
						<label style="padding-top:7px;">{{ $user->name }}</label>
						<strong class="mr-auto"></strong>
						<small class="text-muted"><button class="btn text-danger btn-link btn-sm"><i class="fas fa-minus-circle"></i></button></small>
					</form>
				</div>
				@endforeach
				@else
				<div class="toast-header text-dark list-group-item" style="padding: .75rem 1.25rem;"><h5 class="mb-1 text-secondary display-1"  style="font-size: 20px; ">No hay ningún responsable asignado</h5><strong class="mr-auto"></strong></div>
				@endif
			</ul>           
		</div>
		<div class="col-md-7 col-lg-7">
			<ul class="list-group buttons">
				<div class="toast-header bg-light text-dark list-group-item">
					<h1 class="display-1 mt-2" style="font-size: 20px">Asuntos para atender este tema</h1>
					<strong class="mr-auto"></strong>
					<small class="text-muted"><a href="#agrega_asunto" class="text-danger" data-toggle="collapse" aria-expanded="false" aria-controls="agrega_asunto"><i class="fas fa-plus-circle" ></i> Agregar</a></small>
				</div>

				<div class="text-dark list-group-item collapse" style="padding: .75rem 1.25rem; max-width: 100%" id="agrega_asunto">
					<form method="POST" action="{{ route('services.addSubject') }}" id="form-subjects">
						{!! csrf_field() !!}
						<input name="service_id" class="form-control form-control-sm"  value="{{ $service->id }}" hidden>

						<div class="form-group">
							<input name="subject" id="inputSubject" class="form-control form-control-sm" placeholder="Ingresa un nuevo asunto">
						</div>
						<div>
							<button type="button" class="btn btn-secondary btn-sm" data-toggle="collapse" href="#agrega_asunto" aria-expanded="false" aria-controls="agrega_asunto">Cancelar</button>
							<button type="submit" class="btn btn-danger btn-sm">Guardar</button>
						</div>

					</form>
				</div>
				@if($service->subjects->isNotEmpty())
				@foreach($service->subjects as $subject)
				<div class="toast-header text-dark list-group-item" style="padding: .75rem 1.25rem;">
					<form method="POST" action="{{ route('services.destroySubject', $subject->id) }}" class="form-subject-delete">
						{!! csrf_field() !!}
						{{ method_field('DELETE') }}
						<label>{{ $subject->name }}</label><button type="submit" class="btn text-danger btn-link btn-sm"><i class="fas fa-minus-circle"></i></button>
					</form>
				</div>
				@endforeach
				@else
				<div class="toast-header text-dark list-group-item" style="padding: .75rem 1.25rem;">
					<h5 class="mb-1 text-secondary display-1"  style="font-size: 20px; ">No hay ningún asunto registrado</h5><strong class="mr-auto"></strong>
				</div>
				@endif
			</ul>           
		</div>
	</div>
</div>


<div class="modal fade" id="mSuccess" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header bg-light text-success">
				<h5 class="display-1 mt-2" style="font-size: 20px;" id="modalLabel">Éxito</h5>
				<a href="{{ route("services") }}" role="button" class="close">
					<span aria-hidden="true">&times;</span>
				</a>
			</div>
			<div class="modal-body">
				<h1 class="display-1" style="font-size: 20px;">Se ha registrado correctamente el servicio.</h1>
			</div>
			<div class="modal-footer">
				<a href="{{ route("services") }}" role="button" class="btn btn-sm btn-secondary">Aceptar</a>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="mEditNameService" tabindex="-1" role="dialog" aria-labelledby="mEditNameServiceTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="mEditNameServiceTitle">Editar campo</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form method="POST" action="{{ route('services.updateNameService', $service->id) }}" id="form-service">
				{!! csrf_field() !!}
				{{ method_field('PUT') }}

				<div class="modal-body">
					<div class="form-group">
						<input class="input-service-id" name="service_id" hidden>
						<label for="message-text" class="col-form-label">Nombre del servicio</label>
						<input type="text" class="form-control" id="input-name" name="name">
						<div class="invalid-feedback mb-2 mt-2 error-message-service"></div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-light" data-dismiss="modal">Cancelar</button>
					<button type="submit" class="btn btn-danger">Guardar</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade" id="mEditDescriptionService" tabindex="-1" role="dialog" aria-labelledby="mEditNameServiceTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="mEditNameServiceTitle">Editar campo</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form method="POST" action="{{ route('services.updateDescriptionService', $service->id) }}" id="form-service">
				{!! csrf_field() !!}
				{{ method_field('PUT') }}
				<div class="modal-body">
					<input class="input-service-id" name="service_id" hidden>
					<div class="form-group">
						<label for="input-description" class="col-form-label">Descripción del servicio</label>
						<textarea name="description" class="form-control" id="input-description"></textarea>
						<div class="invalid-feedback mb-2 mt-2 error-message-description"></div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-light" data-dismiss="modal">Cancelar</button>
					<button type="submit" class="btn btn-danger">Guardar</button>
				</div>
			</form>
		</div>
	</div>
</div>


@endsection
@section('script')
@routes
<script type="text/javascript">
	$(document).ready(function() {  
		$('#btn-edit-name').click(function(){
			$('.input-service-id').val({{ $service->id }});
			$('#input-name').val('{{ $service->name }}');
		});

		$('#btn-edit-description').click(function(){
			$('.input-service-id').val({{ $service->id }});
			$('#input-description').val('{{ $service->description }}');
		});
	});

	function detalles(id, name){
		service_id = id;
		$('.service_id').val(service_id);
		$('#labService').html(name+' <a href="#" class="text-danger" data-toggle="modal" data-target="#mEditNameService"><i class="fas fa-edit fa-xs" style="font-size:13px;"></i></a>');
		renderSubjects();
		renderUsers();
	}


	function openModalService() {
		$('.error-message-service').empty();
		$('#service').removeClass('is-invalid');
	}

</script>
@endsection