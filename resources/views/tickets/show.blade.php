@extends('layout')
@section('content')

<style>
.bg-secondary{
	background-color: #6c757db5 !important;
}
.bg-success{
	background-color: #edffde !important;
}
.text-secondary{
	color: #53595f !important;
}
</style>
<div class="container">
	<main role="main" class="col-md-12 col-lg-12 pt-4 px-2 contentCruds">
		<div class="row">
			<div class="col-md-4 col-lg-4 mt-2">
				<div class="{{-- position-fixed --}}" style="max-width: 100%;">
					<ul class="list-group">
						<li class="list-group-item bg-light">
							<h1 class="display-1 mt-2" style="font-size: 25px">
								Ticket #{{ $ticket->id }}
							</h1>
						</li>
						<li class="list-group-item">
							<div class="d-flex w-100 justify-content-between">
								<h5 class="mb-1">Tema:</h5>
							</div>
							<p class="mb-1">{{ $ticket->service->name }}</p>
						</li>
						<li class="list-group-item">
							<div class="d-flex w-100 justify-content-between">
								<h5 class="mb-1">Asunto:</h5>
							</div>
							<p class="mb-1">{{ $ticket->subject }}</p>
						</li>
						<li class="list-group-item">
							<div class="d-flex w-100 justify-content-between">
								<h5 class="mb-1">De:</h5>
							</div>
							<p class="mb-1">{{ $ticket->user->name }} <br> ({{ $ticket->user->email }})
							</p>
						</li>
						<li class="list-group-item">
							<div class="d-flex w-100 justify-content-between">
								<h5 class="mb-1">Para:</h5>
							</div>
							<p class="mb-1">Departamento de {{ $ticket->service->department->name }}</p>
						</li>
						<li class="list-group-item">
							<div class="d-flex w-100 justify-content-between">
								<h5 class="mb-1">Fecha:</h5>
							</div>

							<p class="mb-1">{{ $ticket->getCreatedAt($ticket->created_at)->format('d F Y h:i:s A') }}</p>
						</li>
						<li class="list-group-item">
							<div class="d-flex w-100 justify-content-between">

								@if($ticket->status!='Cerrado')
								<a role="button" href="{{ url(isset($user_tickets) ? '/tickets/admin' : '/tickets') }}" class="btn btn-light text-danger btn-sm " style="width: 48%" id="btnBack">Volver a tickets</a>
								<button type="button" class="btn btn-danger btn-sm" id="btnClose" style="width: 48%">Cerrar ticket</button>

								@else
								<a role="button" href="{{ url(isset($user_tickets) ? '/tickets/admin' : '/tickets') }}" class="btn btn-light text-danger btn-sm btn-block"  id="btnBack">Volver a tickets</a>
								
								@endif
							</div>
						</li>
					</ul>
				</div>
			</div>

			<div class="col-md-8 col-xs-12 ">

				@if($ticket->status!='Cerrado')

				<div class="row mt-2 mb-3">
					<div class="col-10 pr-0">
						<button class="btn btn-secondary btn-block text-left mr-0" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
							Responder &nbsp;<i class="fas fa-envelope"></i>
							<div class="float-right">
								<i class="fas fa-plus"></i>
							</div>
						</button>
					</div>

					<div class="col-2">
						<div class="dropdown">
							<button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="width: 100%">
								Opc
							</button>
							<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
								<a class="dropdown-item" href="#"><i class="fas fa-exchange-alt"></i> Escalar</a>
								<a class="dropdown-item" href="#"><i class="fas fa-share"></i> Compartir</a>
							</div>
						</div>
					</div>
				</div>

				@else

				<div class="btn btn-light text-danger d-block mt-2 pt-2 pb-0 mb-3"><h1 class="display-1" style="font-size: 23px">Ticket cerrado</h1></div>


				@endif

				<div class="collapse mt-2 mb-3" id="collapseExample">
					<div class="card card-body" style="box-shadow: 0 0.25rem 0.75rem rgba(0,0,0,.1);">
						<form method="POST" id="form-message" action="{{ url("tickets/{$ticket->id}") }}">
							{!! csrf_field() !!}
							<div class="form-row">
								<div class="form-group col-md-6">
									<label for="inputName">Nombre</label>
									<input type="text" class="form-control" name="name" id="inputName" value="{{ auth()->user()->name }}" readonly="true">
								</div>
								<div class="form-group col-md-6">
									<label for="inputPassword">Dirección de Email</label>
									<input type="text" class="form-control" name="email" id="inputEmail" value="{{ auth()->user()->email }}" readonly="true">
								</div>
							</div>
							<div class="form-group">
								<label for="inputAsunto">Mensaje</label>
								<textarea class="form-control" name="message" id="inputMessage"></textarea>
								@if($errors->has('message'))
								<p style="font-size: 12px; color: red;">{{ $errors->first('message') }}</p>
								@endif
								<div class="invalid-feedback mb-2 mt-2 error-message"></div>
							</div>
							<div class="form-group">
								<label for="inputAdjunto">Adjuntos</label>
								<input type="file" class="form-control input-sm" name="file" id="inputFile" lang="es" style="font-size: 13px;" multiple>
							</div>
							<div class="float-right">
								<a href="{{ route('tickets') }}" class="btn btn-light">Cancelar</a>
								<button type="submit" class="btn  btn-secondary" id="button-enviar">Enviar</button>
							</div>
						</form>
					</div>
				</div>

				@foreach($ticket->messages as $message)

				<div class="toast fade show col {{ (auth()->user()->id == $message->user->id) ? 'bg-success' : ''}}" role="alert" aria-live="assertive" aria-atomic="true" style="max-width: 100%">

					<div class="toast-header text-secondary pt-2 pb-2 {{ (auth()->user()->id == $message->user->id) ? 'bg-success' : ''}}"">
						<img src="{{ asset($message->user->picture) }}" class="rounded-circle" alt="Usuario" style="margin-right: 8px"/> &nbsp;
						<strong class="mr-auto">{{ (auth()->user()->id == $message->user->id) ? 'Yo' : $message->user->name}}</strong>
						<small class="text-muted">{{ $message->created_at->diffForHumans() }}</small>
					</div>
					<div class="toast-body pt-0">
						<p class="card-text"><?php echo $message->message; ?></p>
						@if(!empty($message->files->first()))

						<div class="card-header pt-1 pb-1 pl-2" style="max-width: 100%">
							<small><i class="fas fa-paperclip"></i>
							Adjuntos:</small>
						</div>
						<div class="card-body pt-2 pb-2">
							<div class="row">
								@foreach($message->files as $file)
								<div class="col-12">
									<?php echo $file->type=='pdf' ? '<i class="fas fa-file-pdf text-danger"></i>' : '<i class="far fa-file-image text-danger"></i>' ?>
									<small><a href="{{ route('tickets.downloadFile', $file->id) }}" class="card-title text-danger">{{ $file->name }}</a></small>
								</div>

								@endforeach
							</div>
						</div>
						@endif
					</div>
				</div>

				@endforeach
			</div>
		</div>
	</main>
</div>

<!-- Modal -->
<div class="modal fade" id="mSuccess" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header bg-light text-success">
				<h5 class="display-1 mt-2" style="font-size: 20px;" id="modalLabel">Mensaje satisfactorio</h5>
				<a href="{{ url("tickets/{$ticket->id}") }}" role="button" class="close">
					<span aria-hidden="true">&times;</span>
				</a>
			</div>
			<div class="modal-body">
				<h1 class="display-1" style="font-size: 20px;">Gracias por utilizar el servicio técnico de Ammmec S.A. de C.V. En un momento se te atenderá.</h1>
			</div>
			<div class="modal-footer">
				<a href="{{ url("tickets") }}" role="button" class="btn btn-sm btn-light">Volver a tickets</a>
				<a href="" role="button" class="btn btn-sm btn-secondary">Aceptar</a>
			</div>
		</div>
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="mClose" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header bg-light text-danger">
				<h5 class="display-1 mt-2" style="font-size: 22px;" id="modalLabel">Cerrar ticket</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<h1 class="display-1 ml-0 " style="font-size: 20px;">¿Está seguro que decesa cerrar esté ticket?</h1>
			</div>
			<div class="modal-footer">
				<a href="{{ url("tickets/{$ticket->id}/cerrar") }}" role="button" class="btn btn-sm btn-secondary">Confirmar</a>
			</div>
		</div>
	</div>
</div>

@endsection
@section('script')
@routes
<script type="text/javascript">
	$(document).ready(function(){
		CKEDITOR.replace( 'inputMessage' );

		$('form').submit(function(){
			$('#button-enviar').addClass('disabled');
			$('#button-enviar').html(' <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span> Cargando...');
		});

		$('#form-message').submit(function(e){
			e.preventDefault();

			var dataForm = new FormData();
			var message = CKEDITOR.instances['inputMessage'].getData();
			
			$.each($('#inputFile')[0].files, function(i, file) {
				dataForm.append('file[]', file);
			});

			dataForm.append('message', message);
			dataForm.append('_token', "{{ csrf_token() }}");

			$.ajax({
				url: 	$(this).attr('action'),
				method: $(this).attr('method'),          
				cache: false,
				contentType: false,
				processData: false,
				data: dataForm, 
				success: function(data){
					$('#mSuccess').modal('toggle');
				},
				error: function (data) {
					stopSpinner();
					$('.error-message').empty();
					if (data.responseJSON.errors.message) {
						$('#inputMessage').addClass('is-invalid');
						$('.error-message').append('<p>'+data.responseJSON.errors.message+'</p>');
					}
				}
			});
		});

		$('#btnClose').click(function(){
			$('#mClose').modal('toggle');
		});
	});

	function stopSpinner() {
		$('#button-enviar').removeClass('disabled');
		$('#button-enviar').html('Enviar');
	}
</script>
@endsection