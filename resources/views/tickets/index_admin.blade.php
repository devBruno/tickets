@extends('layout')
@section('content')
<link rel="stylesheet" href="{{ asset('css/tickets/tickets.css') }}">
<div class="container">
	<div class="row">
		<div class="col-md-3 col-lg-3 mt-4">
			<ul class="list-group buttons">
				
				<li class="list-group-item bg-light">
					<h1 class="display-1 mt-2" style="font-size: 25px">Ver</h1>
				</li>

				<li class="list-group-item">
					<input id="radiobtn_1" class="radiobtn" name="status" type="radio" value="All" tabindex="1" checked>
					<span></span>
					<label for="radiobtn_1">Todos</label>
				</li>

				<li class="list-group-item">
						<input id="radiobtn_2" class="radiobtn" name="status" type="radio" value="Nuevo" tabindex="1">
					<span></span>
					<label for="radiobtn_2">Nuevos</label>
				</li>

				<li class="list-group-item">
						<input id="radiobtn_3" class="radiobtn" name="status" type="radio" value="Abierto" tabindex="1">
					<span></span>
					<label for="radiobtn_3">Abiertos</label>
				</li>

				<li class="list-group-item">
					<input id="radiobtn_4" class="radiobtn" name="status" type="radio" value="Contestado" tabindex="1">
					<span></span>
					<label for="radiobtn_4">Contestados</label>
				</li>
				<li class="list-group-item">
					<input id="radiobtn_5" class="radiobtn" name="status" type="radio" value="Cerrado" tabindex="1">
					<span></span>
					<label for="radiobtn_5">Cerrados</label>
				</li>
			</ul>
		</div>
		<div role="main" class="col-md-9 col-lg-9 mt-4 px-2 {{-- toast fade show pt-2 pb-4 pr-3 pl-3 --}}">
			<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center mt-1 pb-2 mb-2">
				<h1 class="display-4 text-danger" style="font-size: 40px;">Tickets <span style="font-size: 25px;" class="text-dark"> Bandeja de entrada</span></h1>
				<div class="btn-toolbar mb-2 mb-md-0">
					<a href="{{ route('tickets.create_admin') }}" class="btn btn-sm btn-outline-danger">
							<i class="fas fa-sticky-note"></i>
						Abrir ticket
					</a>
				</div>
			</div>
			@if($user_tickets->isNotEmpty())
			<table class="table table-hover" id="table-tickets">
				<thead>
					<tr>
						<th scope="col" width="5%">#</th>
						<th scope="col" width="25%">Usuario</th>
						<th scope="col" width="40%">Asunto</th>
						<th scope="col" width="10%">Estado</th>
						<th scope="col" width="20%"><center>Última act.</center></th>
					</tr>
				</thead>
				<tbody>

				</tbody>
				<tfoot>
					<tr>
						<th scope="col">#</th>
						<th scope="col">Usuario</th>
						<th scope="col">Asunto</th>
						<th scope="col">Estado</th>
						<th scope="col"><center>Última act.</center></th>
					</tr>
				</tfoot>
			</table>
			@else
			<div class="row justify-content-center">
				<div>
					<img src="{{ asset('images/system/sin_tickets_admin.png') }}" alt="sin_tickets_admin" class="center-block imag">
				</div>
			</div>
			@endif
		</div>
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="mEliminar" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form id="form-eliminar" class="frmEliminar" action="?c=clientes&a=Baja" method="POST" enctype="multipart/form-data" name="frmaltaClientes" id="frmEliminar">
				<div class="modal-header">
					<h4 class="modal-title text-danger" id="modalLabel">Eliminar cliente</h4>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<h6>¿Esta seguro que desea eliminar el cliente?</h6>
					<input type="" id="txtIdClienteE" name="idCliente">
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
					<button type="submit" class="btn btn-danger">Eliminar</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection

@section('script')
@routes
<script type="text/javascript">
	$(document).ready(function() {
		status = "All";
		dataTable(status);
		
		$('.radiobtn').click(function() {
			status = $('input:radio[name=status]:checked').val();
			dataTable(status);
			console.log(status);
		});

		// $('*[data-href]').on('click', function() {
		// 	window.location = $(this).data("href");
		// });

	});

	function dataTable(status)
	{
		var table = $('#table-tickets').DataTable( {
			paging:   true,
			ordering: true,
			info:     true,
			filter:   true,
			processing: true,
			serverSide: true,
			destroy: true,
			scrollX: true,
			order: [[ 0, "desc" ]],
			language: {
				lengthMenu: "Mostrar _MENU_ registros por página",
				zeroRecords: "No se encontró ningún registro",
				info: "Mostrando página _PAGE_ de _PAGES_",
				infoEmpty: "No hay registros disponibles",
				infoFiltered: "(Filtrado de _MAX_ registros)",
				search: "Buscar:",
				processing: "Procesando...",
				paginate: {
					first:      "Inicio",
					last:       "Fin",
					next:       "Siguiente",
					previous:   "Anterior"
				},
			},
			ajax:  {
				url: route('tickets.getTickets_admin', [status])
			},
			columns: [
			{ data: 'id', name: 'id' },
			{ data: 'user_name', name: 'user_name'},
			{ data: 'subject', name: 'subject' },
			{ data: 'status_admin', name: 'status_admin' },
			{ data: 'updated_at', name: 'updated_at'}
			],
			search: {
				"regex": true
			}
		});
		show_ticket("#table-tickets tbody", table);
	} 

	var show_ticket = function(tbody, table){
		$(tbody).on("click", "tr", function(){
			idTicket=$(this).attr('id');
			window.location=route('tickets.show_admin', [idTicket]);
		})
	}

</script>
@endsection