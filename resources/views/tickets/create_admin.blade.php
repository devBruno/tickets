@extends('layout')
@section('content')
<link rel="stylesheet" href="{{ asset('css/tickets/tickets.css') }}">

<style type="text/css" media="screen">
</style>
<div class="container">
	<main role="main" class="col-md-12 col-lg-12 pt-4 px-2 contentCruds">

		<div class="row">

			<div class="col-md-4 col-lg-4">
				<div class="list-group">
					<div class="list-group-item list-group-item-action bg-light">
						<h1 class="display-1 mt-2" style="font-size: 25px">
							Tickets recientes
						</h1>
					</div>
					@forelse (auth()->user()->tickets_user_limit as $ticket)
					<a href="{{ url("tickets/{$ticket->id}") }}" class="list-group-item list-group-item-action head-dark">
						<div class="d-flex w-100 justify-content-between">
							<h5 class="mb-1"># {{ $ticket->id }} </h5>
							<small> {{ $ticket->getCreatedAt($ticket->created_at)->format('d F Y') }} </small>
						</div>
						<p class="mb-1"> <b>Asunto: </b>{{ $ticket->subject }} </p>
						<p class="mb-1"> <b>De: </b>{{ $ticket->user->name }} </p>

						@if($ticket->status === "Abierto")
						<span class="badge badge-success">{{ $ticket->status }}</span>
						@else
						<span class="badge badge-secondary pull-right">{{ $ticket->status }}</span>
						@endif
					</a>
					@empty
					<div class="list-group-item head-dark">
						<div class="d-flex w-100 justify-content-between">
							<h5 class="mb-1 text-secondary display-1"  style="font-size: 20px; padding-left:40px;">No hay historial de tickets </h5>
						</div>
					</div>
					@endforelse
					<div class="list-group-item">
						<a href="{{ url("tickets/admin") }}" class="btn btn-sm btn-light text-danger btn-block" role="button">Mostrar todos</a>
					</div>
				</div>
			</div>

			@if (session()->has('success'))

			<div class="col-8 col-xs-12 toast fade show pt-3 pb-4 pr-4 pl-4" style="max-width: 100%; max-height: 300px;">
				<div class="mb-3">
					<h1 class="display-4 text-danger mb-0" style="font-size: 40px;">Nuevo Ticket <span style="font-size: 25px;" class="text-dark">Desde departamento de {{ auth()->user()->department->name }}</span></h1>
				</div>

				<div class="alert alert-success text-center">
					<h1 class="display-1 mt-2 bold" style="font-size: 20px;">{{ session('success') }}</h1>
				</div>
				<div class="text-center">
					<p class="display-1" style="font-size: 20px;">Se ha creado correctamente el ticket<br></p>
					<a href="{{ route('tickets.show_admin', session('idTicket')) }}" role="button" class="btn btn-light">Ver ticket <i class="fas fa-arrow-circle-right"></i></a>
				</div>
			</div>

			@else

			<div class="col-8 col-xs-12 toast fade show pt-3 pb-4 pr-4 pl-4" style="max-width: 100%">
				<div class="mb-3">
					<h1 class="display-4 text-danger mb-0" style="font-size: 40px;">Abrir ticket <span style="font-size: 25px;" class="text-dark">Desde departamento de {{ auth()->user()->department->name }}</span></h1>
				</div>

				<form method="POST" action="{{ url('tickets') }}" enctype="multipart/form-data" class="md-form" id="form-message">
					{!! csrf_field() !!}
					
					<div class="form-row">
						<div class="form-group col-md-6">
							<label for="inputName">Nombre</label>
							<select name="user_id" id="inputName" class="form-control user_id" autofocus>
								<option value="">Seleccione ...</option>
								@foreach($users_in_open_ticket as $user)
								<option value="{{ $user->id }}" {{ old('user_id') == $user->id ? 'selected' : '' }}>{{ $user->name }} - {{ $user->email }}</option>
								@endforeach
							</select>
							<div class="text-danger mb-2 mt-2 error-user_id"></div>
						</div>

						<div class="form-group col-md-6">
							<label for="inputPassword">Dirección de Email</label>
							<input type="text" class="form-control" name="email" id="inputEmail" placeholder="Seleccione un usuario" readonly="true">
						</div>
					</div>
					
					<div class="form-row">
						<div class="form-group col-md-6">
							<label for="departments">Departamento</label>
							<select id="departments" name="department_id" class="form-control department_id">
								<option value=" {{ $departments->id }} " {{ old('department_id') == $departments->id ? 'selected' : '' }}> {{ $departments->name }} </option>
							</select>
							<div class="text-danger mb-2 mt-2 error-department_id"></div>
						</div>
						<div class="form-group col-md-6 mb-0">
							<label for="services">Tema relacionado</label>
							<select id="services" name="service_id" class="form-control service_id" onchange="changeSubject()">
								<option value="">Seleccione ...</option>
								@forelse ($services_dep as $service)
								<option value=" {{ $service->id }} " {{ old('service_id') == $service->id ? 'selected' : '' }}> {{ $service->name }} </option>
								@empty
								<option>No hay servicios registrados</option>
								@endforelse
							</select>
							<div class="text-danger mb-2 mt-2 error-service_id"></div>
						</div>
					</div>

					<div class="form-group">
						<div class="d-flex justify-content-between mb-0 " >
							<label for="inputSubject" id="labSubject" class="pb-0">Asunto</label>
							
							<ul class="buttons" style="margin-bottom: 0px; margin-top: -4px;">
								<li class="">
									<input id="radiobtn_1" class="radiobtn" name="filter" type="checkbox" value="All" tabindex="1">
									<span></span>
									<label for="radiobtn_1">Elegir otro asunto</label>
								</li>
							</ul>	
						</div>

						<select type="text" id="selectSubject" class="form-control pt-0 subject" name="subject" value="{{ old('subject') }}">
							<option value="">Seleccione un tema relacionado ...</option>
						</select>

						<input type="text" id="inputSubject" class="form-control subject" name="subject" value="{{ old('subject') }}">

						<div class="text-danger mb-2 mt-2 error-subject"></div>
					</div>

					<div class="form-group">
						<label for="inputAsunto">Mensaje</label>
						<textarea class="form-control message" name="message" id="inputMessage">{{ old('message') }}</textarea>
						<div class="text-danger mb-2 mt-2 error-message"></div>
					</div>
					

					<div class="form-group">
						<label for="inputAdjunto">Adjuntos</label>
						<div class="row">
							<div class="col-sm-12">
								<input type="file" class="form-control input-sm" name="file" id="archivo" lang="es" style="font-size: 13px;" multiple>
							</div>
						</div>
					</div>
					<div class="float-right mt-2">
						<a href="{{ route('tickets') }}" class="btn btn-light">Cancelar</a>		
						<button type="submit" class="btn btn-danger" id="button-enviar">Enviar</button>
					</div>
				</form>
				@endif
			</div>
		</div>
	</main>
</div>
@endsection

@section('script')
@routes
<script>
	var typeSubject = "selectSubject";
	CKEDITOR.replace( 'inputMessage' , {});
	CKEDITOR.editorConfig = function( config ) {
		config.language = 'es';
		config.uiColor = '#F7B42C';
		config.height = 300;
		config.toolbarCanCollapse = true;
	};

	$(document).ready(function(){
		changeSubject();
		verificar_checked();
		
		$('form').submit(function(){
			$('#button-enviar').addClass('disabled');
			$('#button-enviar').html(' <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span> Cargando...');
		});

		$('#radiobtn_1').change(function(){
			verificar_checked();
		});

		$('#form-message').submit(function(e){
			e.preventDefault();

			var dataForm = new FormData();
			var user_id = $('select[name=user_id]').val();
			var department_id = $('select[name=department_id]').val();
			var service_id = $('select[name=service_id]').val();
			var subject = $('#'+typeSubject).val();
			var message = CKEDITOR.instances['inputMessage'].getData();

			$.each($('#archivo')[0].files, function(i, file) {
				dataForm.append('file[]', file);
			});

			dataForm.append('user_id', user_id);
			dataForm.append('department_id', department_id);
			dataForm.append('service_id', service_id);
			dataForm.append('subject', subject);
			dataForm.append('message', message);
			dataForm.append('_token', "{{ csrf_token() }}");

			$.ajax({
				url: 	$(this).attr('action'),
				method: $(this).attr('method'),          
				cache: false,
				contentType: false,
				processData: false,
				data: dataForm, 
				success: function(ticket_id){
					window.location.href = route('tickets.created_admin', [ticket_id]);
				},
				error: function (data) {
					cleanInputsSuccess();
					stopSpinner();
					for (var [campo, message] of Object.entries(data.responseJSON.errors)){
						$('.error-'+campo).empty();
						$('.'+campo).addClass('is-invalid');
						$('.error-'+campo).append('<small><p>'+message+'</p></small>');
					}
				}
			});
		});

		$( "#inputName" ).change(function() {
			var userId = this.value;
			$.get(route('users.getUser', 
				{ user: userId }), 
			function(user){
				var user = JSON.parse(user);
				$('#inputEmail').empty();
				$('#inputEmail').val(user.email);
			});
		});

		// $( "#departments" ).change(function() {
		// 	var departmentId = this.value;
		// 	$.get(route('tickets.listServices', 
		// 		{ departmentId: departmentId }), 
		// 	function(service){
		// 		var serviceJSON = JSON.parse(service);
		// 		$("#services").empty();
		// 		var selector = document.getElementById("services");
		// 		selector.options[0] = new Option("Seleccione ...","");
		// 		for (var i in serviceJSON) {
		// 			var j = parseInt(i) + 1;
		// 			selector.options[j] = new Option(serviceJSON[i].nombre,serviceJSON[i].ids);
		// 			selector.options[j].title = serviceJSON[i].descripcion;
		// 		}
		// 	});
		// });

		// $( "#services" ).change(function() {
		// 	var serviceId = this.value;
		// 	$.get(route('services.listSubjects', 
		// 		{ service_id: serviceId }), 
		// 	function(subject){
		// 		var subjectJSON = JSON.parse(subject);
		// 		$("#selectSubject").empty();
		// 		var selector = document.getElementById("selectSubject");
		// 		if($.isEmptyObject(subjectJSON)){
		// 			selector.options[0] = new Option("No hay asuntos registrados","");
		// 		}else{
		// 			selector.options[0] = new Option("Seleccione ...","");
		// 			for (var i in subjectJSON) {
		// 				var j = parseInt(i) + 1;
		// 				selector.options[j] = new Option(subjectJSON[i].nombre,subjectJSON[i].ids);
		// 			}
		// 		}
		// 	});
		// });
		// 
		
	});

	function stopSpinner() {
		$('#button-enviar').removeClass('disabled');
		$('#button-enviar').html('Enviar');
	}

	function changeSubject() {
		var serviceId = $('select[name=service_id]').val();
		$.get(route('services.listSubjects', 
			{ service_id: serviceId }), 
		function(subject){
			var subjectJSON = JSON.parse(subject);
			$("#selectSubject").empty();
			var selector = document.getElementById("selectSubject");
			if($.isEmptyObject(subjectJSON)){
				selector.options[0] = new Option("No hay asuntos registrados","");
			}else{
				selector.options[0] = new Option("Seleccione ...","");
				for (var i in subjectJSON) {
					var j = parseInt(i) + 1;
					selector.options[j] = new Option(subjectJSON[i].nombre,subjectJSON[i].ids);
				}
			}
		});
	}

	function cleanInputsSuccess() {
		var campos = ['service_id', 'department_id', 'subject', 'message'];
		for (var i in campos){
			$('.error-'+campos[i]).empty();
			$('.'+campos[i]).removeClass('is-invalid');
		}
	}

	verificar_checked=function(){
		if($('#radiobtn_1').is(':checked')){
			typeSubject = "inputSubject";
			$('#inputSubject').attr({type:'text'});
			$('#inputSubject').focus();
			$('#selectSubject').hide();
			$("#inputSubject").prop( "disabled", false );
			$("#selectSubject").prop( "disabled", true );
		}
		else{
			typeSubject = "selectSubject";
			$('#inputSubject').attr({type:'hidden'});
			$('#selectSubject').show();
			$('#selectSubject').val("");
			$("#inputSubject").prop( "disabled", true );
			$("#selectSubject").prop( "disabled", false );
		}
	}
</script>
@endsection



