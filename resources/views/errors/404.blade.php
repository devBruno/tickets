@extends('layout')

@section('title', 'Página no encontrada')
@section('content')
<div class="row justify-content-center" style="margin-top: 15%">
		<img src="{{ asset('images/system/error_404.png') }}" alt="sin_tickets_admin" class="center-block imag">
</div>
@endsection
