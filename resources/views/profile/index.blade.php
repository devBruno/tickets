@extends('layout')
@section('content')
<style>
.image_outer_container{
	margin-top: auto;
	margin-bottom: auto;
	border-radius: 50%;
	position: relative;
}

.image_inner_container{
	border-radius: 50%;
	padding: 5px;
	background: #833ab4; 
	background: -webkit-linear-gradient(to bottom, rgb(106, 115, 123), rgb(69, 85, 95), rgb(227, 27, 35), #912020); 
	background: linear-gradient(to bottom,  rgb(106, 115, 123), rgb(69, 85, 95), rgb(227, 27, 35), #912020);
}
.image_inner_container img{
	height: 200px;
	width: 200px;
	border-radius: 50%;
	border: 5px solid white;
}

.image_outer_container .green_icon{
	background-color: rgb(64, 177, 56);
	position: absolute;
	right: 30px;
	bottom: 10px;
	height: 30px;
	width: 30px;
	border:5px solid white;
	border-radius: 50%;
}

.text-secondary{
	color: #52595f !important;
}

.jumbotron{
	margin-bottom: 1rem !important; 
}
body{
	background-color: #f8f9fa !important;
}

</style>

<div class="container pt-3">

	<div class="row">
		<div class="col-4">
			<div class="toast fade show col pb-3" role="alert" aria-live="assertive" aria-atomic="true">
				<div class="jumbotron jumbotron-fluid border mt-3" style="padding-top: 25px; padding-bottom: 5px">
					<div class="d-flex justify-content-center h-100">
						<div class="image_outer_container">
							<div class="green_icon"></div>
							<div class="image_inner_container">
								<img src="{{ asset(auth()->user()->picture) }}">
							</div>
						</div>
					</div>
					{{-- <small><a class="text-danger ml-4" data-toggle="collapse" href="#cambiarImagen" role="button" aria-expanded="false" aria-controls="cambiarImagen">
						<i class="fas fa-image"></i> Cambiar imagen
					</a></small> --}}
					<div class="collapse mt-2 mb-2" id="cambiarImagen">
						<div class="card card-body">
							<form method="POST" action="{{ url('/perfil/updateImage') }}" enctype="multipart/form-data" id="form-image">
								{{ method_field('PUT') }}
								{!! csrf_field() !!}

								{{-- <div class="form-group" style="margin-bottom: 5px;">
									<p class="help-block" for="image">Seleccione su imagen</p>
									<input type="file" class="form-control-file" id="inputImage" name="image">
								</div> --}}
								<div class="error-image"></div>

								<button type="button" class="btn btn-secondary btn-sm" data-toggle="collapse" href="#cambiarImagen" aria-expanded="false" aria-controls="cambiarImagen">Cancelar</button>
								<button type="submit" class="btn btn-danger btn-sm">Guardar</button>
							</form>
						</div>
						
					</div>


					<h1 class="display-1 text-center text-secondary pl-1 pr-1 pb-1 mt-3" style="font-size: 25px;">{{ auth()->user()->name }}</h1>
				</div>
				<div class="toast-header text-secondary">
					<h5 class="text-secondary mt-2"><i class="fas fa-user"></i> Información general</h5>
					{{-- <strong class="mr-auto"></strong>
					<small class="text-muted"><a href="#" class="text-danger" data-toggle="modal" data-target="#profileModal" data-whatever="@getbootstrap" onclick="openModalProfile()"><i class="fas fa-user-edit"></i> Editar</a></small> --}}
				</div>
				<div class="toast-body mt-2 pb-0">
					<div class="col-md-auto">
						<h1 class="display-4 pb-0" style="font-size: 17px;"><strong>Nombre:</strong> <br>{{ auth()->user()->name }}</h1>
					</div>
					<hr class="my-3">
					<div class="col-md-auto">
						<h1 class="display-4" style="font-size: 17px;"><strong>Email:</strong> <br>{{ auth()->user()->email }}</h1>
					</div>
					<hr class="my-3 mb-0">
					<div class="col-md-auto">
						<h1 class="display-4" style="font-size: 17px;"><strong>Departamento:</strong> <br>{{ auth()->user()->department->name }}</h1>
					</div>
					<hr class="my-3 mb-0">
					<div class="col-md-auto">
						<h1 class="display-4" style="font-size: 17px;"><strong>Número de empleado:</strong> <br>{{ auth()->user()->employee_number }}</h1>
					</div>
					{{-- <hr class="my-3 mb-0">
					<div class="col-md-auto text-center">
						<small><a class="text-danger ml-4" data-toggle="collapse" href="#cambiarPassword" role="button" aria-expanded="false" aria-controls="cambiarPassword">
							<i class="fas fa-unlock-alt"></i> Cambiar contraseña
						</a></small>
						<div class="collapse mt-2 mb-2" id="cambiarPassword">
							<div class="card card-body">
								<form method="POST" action="{{ url('/perfil/updatePassword') }}" id="form-updatePass">
									{{ method_field('PUT') }}
									{!! csrf_field() !!}

									<div class="form-group">
										<div class="col-sm-12">
											<input type="password" class="form-control form-control-sm password_old" id="inputpassword_old" placeholder="Contraseña actual" name="password_old">
											<div class="invalid-feedback mb-2 mt-2 error-message-password_old"></div>
										</div>
									</div>
									<div class="form-group">
										<div class="col-sm-12">
											<input type="password" class="form-control form-control-sm password" id="inputpassword" placeholder="Nueva contraseña" name="password">
											<div class="invalid-feedback mb-2 mt-2 error-message-password"></div>
										</div>
									</div>
									<div class="form-group">
										<div class="col-sm-12">
											<input type="password" class="form-control form-control-sm password" id="inputpassword_confirmation" placeholder="Confirmar contraseña" name="password_confirmation">
										</div>
									</div>
									<button type="button" class="btn btn-secondary btn-sm" data-toggle="collapse" href="#cambiarPassword" aria-expanded="false" aria-controls="cambiarPassword">Cancelar</button>
									<button type="submit" class="btn btn-danger btn-sm">Guardar</button>
								</form>
							</div>
						</div>

					</div> --}}
				</div>
			</div>
		</div>
		<div class="col-8">
			<div class="toast fade show col" role="alert" aria-live="assertive" aria-atomic="true" style="max-width: 100%">
				<div class="toast-header text-secondary pt-2">
					<h4 class="text-secondary mt-2"><i class="fas fa-chart-line"></i> Indicadores</h4>
					<strong class="mr-auto"></strong>
				</div>
				<div class="toast-body text-center mt-2 pb-5">
					<div id="chart_div" style="width: 100%; height: 100%;"></div>
					<hr>
					{{-- <div id="piechart_3d" style="width: 900px; height: 500%;"></div> --}}
				</div>

			</div>
		</div>
	</div>
</div>

<!-- Modal para editar la informacion del usuario -->
<div class="modal fade" id="profileModal" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false" tabindex="-1">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form method="POST" action="{{ url('/perfil/updateProfile') }}" id="form-profile">
				{{ method_field('PUT') }}
				{!! csrf_field() !!}

				<div class="modal-header bg-secondary text-light">
					<h5 class="modal-title" id="exampleModalLabel">Editar información</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label for="nombre">Nombre</label>
						<input type="text" class="form-control" id="inputname" placeholder="Nombre" name="name">
						<div class="invalid-feedback mb-2 mt-2 error-message-name"></div>
					</div>

					<div class="form-group">
						<label for="email">Email</label>
						<input type="text" class="form-control" id="inputemail" placeholder="Email" name="email">
						<div class="invalid-feedback mb-2 mt-2 error-message-email"></div>
					</div>

					<div class="form-group">
						<label for="email">Departamento</label>
						<select class="form-control" name="department_id">
							@foreach($departments as $department)
							@if($department->id==auth()->user()->department->id) 
							<option value="{{ auth()->user()->department->id }}" selected>
								&nbsp;{{ auth()->user()->department->name }}
							</option>
							@else
							<option value="{{ $department->id }}">
								&nbsp;{{ $department->name }}
							</option>
							@endif
							@endforeach
						</select>

						{!! $errors->first('department_id', '<div class="invalid-feedback mb-2 mt-2">:message</div>') !!}
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-light" data-dismiss="modal" id="btnCloseProfile">Cancelar</button>
					<button type="submit" class="btn btn-danger">Guardar</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade" id="mSuccess" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header bg-light text-success">
				<h5 class="display-1 mt-2" style="font-size: 20px;" id="modalLabel">Éxito</h5>
				<a href="{{ route("profile") }}" role="button" class="close">
					<span aria-hidden="true">&times;</span>
				</a>
			</div>
			<div class="modal-body">
				<h1 class="display-1" style="font-size: 20px;">Tu información ha sido actualizada exitosamente.</h1>
			</div>
			<div class="modal-footer">
				<a href="{{ route("profile") }}" role="button" class="btn btn-sm btn-secondary">Aceptar</a>
			</div>
		</div>
	</div>
</div>

@endsection
@section('script')
@routes
<script type="text/javascript">
	$(document).ready(function(){
		$('#form-profile').submit(function(e){
			e.preventDefault();
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
				}
			});
			$.ajax({
				url: 	$(this).attr('action'),
				method: $(this).attr('method'),
				data: 	$(this).serialize(),
				success: function(data){
					$('#profileModal').modal('toggle');
					$('#mSuccess').modal('toggle');
					$('#form-profile').reset();
				},
				error: function (data) {
					for (var [campo, message] of Object.entries(data.responseJSON.errors)){
						$('.error-message-'+campo).empty();
						$('#input'+campo).addClass('is-invalid');
						$('.error-message-'+campo).append('<p>'+message+'</p>');
					}
				}
			});
		});

		$('#form-updatePass').submit(function(e){
			e.preventDefault();
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
				}
			});
			$.ajax({
				url: 	$(this).attr('action'),
				method: $(this).attr('method'),
				data: 	$(this).serialize(),
				success: function(data){
					collapsehide();
					$('#form-updatePass')[0].reset();
				},
				error: function (data) {
					cleanInputsPassword();
					for (var [campo, message] of Object.entries(data.responseJSON.errors)){
						$('.error-message-'+campo).empty();
						$('.'+campo).addClass('is-invalid');
						$('.error-message-'+campo).append('<p>'+message+'</p>');
					}
				}
			});
		});

		$('#form-image').submit(function(e){
			e.preventDefault();
			var image = $('#inputImage')[0].files[0];
			if (image) {
				var dataForm = new FormData();
				dataForm.append('image', image);
				dataForm.append('_token', "{{ csrf_token() }}");

				$.ajax({
					url: 	$(this).attr('action'),
					method: $(this).attr('method'),          
					cache: false,
					contentType: false,
					processData: false,
					data: dataForm, 
					success: function(data){
						if(data.is === 'failed'){
							$('.error-image').empty();
							$('.error-image').append('<p align="justify" style="font-size: 11px; color: red;">'+data.error.image+'</p>');
						}else if(data.is === 'success'){
							$('#mSuccess').modal('toggle');
						}
					}
				});
			}else{
				$('.error-image').empty();
				$('.error-image').append('<p align="justify" style="font-size: 11px; color: red;">Introduzca una imagen</p>');
			}
		});
	});

	function collapsehide() {
		$('.collapse').collapse('hide');
		cleanInputsPassword();
	}

	function cleanInputsPassword() {
		$('.error-message-password_old').empty();
		$('.password_old').removeClass('is-invalid');
		$('.error-message-password').empty();
		$('.password').removeClass('is-invalid');
	}

	function openModalProfile() {
		$('#inputname').val('{{auth()->user()->name}}');
		$('#inputemail').val('{{auth()->user()->email}}');
		$('.error-message-name').empty();
		$('.error-message-email').empty();
		$('#inputname').removeClass('is-invalid');
		$('#inputemail').removeClass('is-invalid');
	}

	google.charts.load('current', {'packages':['corechart']});
	google.charts.setOnLoadCallback(drawVisualization);

	function drawVisualization() {

		@if (count($findStatus) != 0)
		console.log("lleno");
		var color = ['#bf2c2c', '#6c757d'];
		var item = 0;
		var data = google.visualization.arrayToDataTable([
			['Estado', 'Total', { role: 'style' }],
			@foreach ($findStatus as $r)
			['{{ $r->status }}',  {{$r->status_count}}, color[item++]],
			@endforeach
			]);

		var options = {
			legend: 'none',
			title : 'TICKETS',
			is3D: true,
			vAxis: {title: 'Número de Tickets'},
			hAxis: {title: 'Estado del Ticket'},
			seriesType: 'bars',
			series: {5: {type: 'line'}}
		};

		var chart = new google.visualization.ComboChart(document.getElementById('chart_div'));
		chart.draw(data, options);

		@else
		console.log("vacio");

		$('#chart_div').html('<h1 class="display-1 text-center text-secondary pl-1 pr-1 pb-1 mt-3" style="font-size: 20px;">No hay indicadores</h1>');
		@endif		
	}
</script>
@endsection