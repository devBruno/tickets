 <!doctype html>
 <html lang="es">
 <head>
 	<meta charset="utf-8">
 	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
 	<meta name="description" content="">
 	<meta name="author" content="">
 	<meta name="csrf-token" content="{{ csrf_token() }}">

 	<link rel="icon" href="{{ asset('images/logo/logoammmec.png') }}">

 	<title>SOPORTE AMMMEC</title>

 	<!-- Bootstrap core CSS -->
 	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">

 	{{-- <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.css" rel="stylesheet"> --}}

 	<link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">

 	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous"> 	

 	<link href="{{ asset('css/dashboard/dashboard.css') }}" rel="stylesheet">

 	{{-- ckeditor --}}
 	{{-- <link href="{{ asset('plugins/ckeditor/samples.css') }}" rel="stylesheet"> --}}
 	
 	<!-- Bootstrap core JS -->
 	<script src="{{ asset('js/jquery-3.1.1.min.js') }}"></script>
 	
 	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
 	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

 	<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

 	<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
 	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
 	<script src="{{ asset('js/select2.min.js') }}"></script>

 	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>


 	<script src="{{ asset('plugins/ckeditor/ckeditor.js') }}"></script>

 	<style>
 		.page-item.active .page-link {
 			z-index: 1;
 			color: #fff;
 			background-color: #dc3545;
 			border-color: #dc3545;
 		}

 		.custom-control-input:checked~.custom-control-label::before {
 			color: #fff;
 			border-color: #dc3545;
 			background-color: #dc3545;
 		}
 		.page-link{
 			color:#18191b;
 		}

 		.radio-filter{
 			background-color: #dc3545;
 			color:#dc3545;
 		}

 		.page-item.disabled .page-link{
 			color: #aaa;
 		}

 		[data-href] {
 			cursor: pointer;
 		}

 		/* Radio button */
 		.radiobtn {
 			display: none;
 		}

 		.buttons li {
 			display: block;
 		}
 		.buttons li label{
 			padding-left: 30px;
 			position: relative;
 			left: -25px;
 		}
 		.buttons li label:hover {
 			cursor: pointer;
 		}
 		.buttons li span {
 			display: inline-block;
 			position: relative;
 			top: 5px;
 			border: 2px solid #ccc;
 			width: 18px;
 			height: 18px;
 			background: #fff;
 		}
 		.radiobtn:checked + span::before{
 			content: '';
 			border: 2px solid #fff;
 			position: absolute;
 			width: 14px;
 			height: 14px;
 			background-color: #dc3545;
 		}
 		.active{
 			background-color: #dc3545 !important;
 		}

 		.alert-bottom{
 			position: fixed;
 			z-index: 3;
 			bottom: 0px;
 			left:1%;
 			width: 98%;
 		}

 		.rounded-circle{
 			min-width: 40px; 
 			min-height: 40px; 
 			max-width: 40px; 
 			max-height: 40px;
 		}

 	</style>
 </head>


 <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark py-0">
 	<div class="container">
 		<a href="../../?c=modulos" class="" style="padding-top: 1px; padding-left: 0px; margin-right: 20px;">
 			<img src="{{ asset('images/logo/logo-blanco.png') }}" alt="" style="max-width: 100px; max-height: 50px;">
 		</a>
 		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
 			<span class="navbar-toggler-icon"></span>
 		</button>

 		<div class="collapse navbar-collapse" id="navbarSupportedContent">
 			<ul class="navbar-nav mr-auto">
 				<li class="nav-item ">
 					<a class="nav-link <?php if(isset($tickets)){ ?> active <?php } ?>" href="{{ route('tickets') }}">
 						<i class="fas fa-ticket-alt"></i>
 						Mis tickets
 					</a>
 				</li>
 				<li class="nav-item ">
 					<a class="nav-link <?php if(isset($abrir_ticket)){ ?> active <?php } ?>" href="{{ route('tickets.create') }}">
 						<i class="fas fa-sticky-note"></i>
 						Abrir ticket
 					</a>
 				</li>
 				@if(auth()->user()->services->isNotEmpty() || auth()->user()->tickets->isNotEmpty())
 				<li class="nav-item">
 					<a class="nav-link <?php if(isset($user_tickets)){ ?> active <?php } ?>" href="{{ route('tickets.admin') }}">
 						<i class="fas fa-envelope"></i>
 						Bandeja de entrada
 					</a>
 				</li>
 				@endif
 				@if(auth()->user()->is_admin=='1')
 				<li class="nav-item">
 					<a class="nav-link <?php if(isset($services)){ ?> active <?php } ?>" href="{{ route('services') }}">
 						<i class="fas fa-clipboard-check"></i>
 						Servicios
 					</a>
 				</li>
 				@endif
 				@if(auth()->user()->is_admin=='0')
 				<li class="nav-item">
 					<a class="nav-link <?php if(isset($profile)){ ?> active <?php } ?>" href="{{ route('profile') }}">
 						<i class="fas fa-user"></i>
 						Mi cuenta
 					</a>
 				</li>	
 				@endif
 				@if(auth()->user()->department_id=='2' && auth()->user()->is_admin==1)
 				<li class="nav-item">
 					<a class="nav-link <?php if(isset($users)){ ?> active <?php } ?>" href="{{ route('users') }}">
 						<span data-feather="shopping-cart"></span>
 						<i class="fas fa-sticky-note"></i>
 						Usuarios
 					</a>
 				</li>
 				@endif	
 			</ul>
 			<ul class="navbar-nav flex-row ml-md-auto">
 				<li class="nav-item float-right dropdown mr-0 mt-2">

 					<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
 						<i class="fas fa-bell"></i>  
 						@if ($count = Auth::user()->unreadNotifications->count()) 
 						<span class="badge badge-danger">{{ $count }}</span> 
 						@endif
 					</a>

 					<ul class="dropdown-menu dropdown-menu-right pt-0 pb-0" style="border: 1px solid #c4c4c4; box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19); width: 380px;">

 						<div class="list-group">
 							@if (Auth::user()->notifications->count()) 
 							@foreach(Auth::user()->notifications_limit as $notification)

 							<a href="{{ route('notifications.read', ['notification_id' => $notification->id, 'route_name' => $notification->data['route_name'], 'id' => $notification->data['id']]) }}" class="{{ $notification->read_at ? 'list-group-item-ligth' : 'list-group-item-secondary' }} pt-2 pb-2 pr-3 list-group-item-action head-dark border-ligth">
 								<div class="row">
 									<div class="col-lg-2">

 										<img src="{{ asset($notification->data['picture']) }}" class="rounded-circle" alt="Usuario" style="margin-left:11px;"/>

 									</div>
 									<div class="col-lg-10">
 										<div class="d-flex w-100 justify-content-between">
 											<p class="mb-0">
 												<small><?php echo $notification->data['text'] ?></small>
 											</p>
 										</div>
 										
 										<small class="">
 											<?php echo $notification->data['icon'] ?>
 											{{ $notification->created_at->diffForHumans() }}
 										</small>
 									</div>
 								</div>
 							</a>
 							@endforeach
 							<a href="{{ route('notifications') }}" class="navbar navbar-light bg-light border justify-content-center text-danger">
 								Mostrar todas
 							</a>
 							@else
 							<div class="navbar navbar-light bg-light border justify-content-center text-danger">
 								No hay notificaciones para mostrar
 							</div>
 							@endif
 						</div>
 					</ul>
 				</li>

 				<li class="nav-item dropdown">
 					<a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
 						<img src="{{ asset(auth()->user()->picture) }}" class="rounded-circle" alt="Usuario" style="min-width: 40px; min-height: 40px; max-width: 40px; max-height: 40px;" />
 						<label class="ml-1">{{ auth()->user()->name }}</label>
 						
 					</a>
 					<div class="dropdown-menu">
 						<a class="dropdown-item" href="{{ route('profile') }}">Mi cuenta</a>
 						<form method="POST" action={{ route('logout') }}>
 							{{ csrf_field() }}
 							<button class="dropdown-item" type="submit">Cerrar sesión</button>
 						</form>
 					</div>
 				</li>

 			</ul>
 		</div>
 	</div> <!-- contailer -->
 </nav>

 
 <div class="tab-content" id="v-pills-tabContent" style="margin-top: 75px; margin-bottom: 35px;">
 	<div class="padding-nav">
 		<div id="mensajejs"></div>
 	</div>
 	@yield('content')
 	@yield('script')
 </div>

 <div class="alert alert-success alert-bottom text-center" role="alert" id="div-success" style="display:none"><strong><font class="font-message"></font></strong></div>

 <div class="alert alert-danger alert-bottom text-center" role="alert" id="div-error" style="display:none"><strong><font class="font-message"></font></strong></div>

</body>
</html>
