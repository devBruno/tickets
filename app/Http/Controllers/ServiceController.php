<?php

namespace App\Http\Controllers;

use DB;
use App\User;
use Validator;
use DataTables;
use App\Service;
use App\Subject;
use Carbon\Carbon;
use App\Department;
use Illuminate\Http\Request;
use Yajra\DataTables\EloquentDataTable;


class ServiceController extends Controller
{

	public function __construct()
	{
		$this->middleware('auth'); 
	}

	function index()
	{
		$departments=Department::all();
		$services=Service::all();
		return view('services.index', compact('services', 'departments'));
	}

	public function show(Service $service)
	{
		$services=true;
		Carbon::setLocale('es');
		return view('services.show', compact('service','services'));
	}

	public function store()
	{
		$data = request()->validate([
			'service' => 'required',
			'description' => 'required',
			'department' => '',
		],[
			'service.required' => 'El campo servicio es obligatorio',
			'description.required' => 'El campo descripción es obligatorio',
		]);
		
		Service::create([
			'name' => $data['service'],
			'status' => 'Activo',
			'description' => $data['description'],
			'department_id' => auth()->user()->department->id,
		]);
	}

	public function updateNameService(Service $service)
	{
		$data=request()->validate([
			'name' => 'required'
		],[
			'name.required' => 'El nombre del servicio es obligatorio',
		]);
		
		$service->update($data);

		return redirect("/servicios/{$service->id}");
	}

	public function updateDescriptionService(Service $service)
	{
		$data=request()->validate([
			'description' => 'required'
		],[
			'description.required' => 'La descripción del servicio es obligatoria',
		]);
		
		$service->update($data);

		return redirect("/servicios/{$service->id}");
		
	}

	public function getServices($status)
	{
		$services = Service::findByStatus($status);
		return Datatables::of($services)
		->editColumn('created_at', function(Service $service){
			Carbon::setLocale('es');
			return $service->created_at->diffForHumans();
		})
		->editColumn('name',function(Service $service){
			return $service->name . ' <a href="'.route('services.show',[$service->id]).'" class="text-danger" ><i class="fas fa-external-link-alt fa-sm"></i></a>';
			// onclick="detalles('.$service->id.','."'".$service->name."'".')"
		})
		->editColumn('status', function(Service $service){
			$status = $service->status;
			$id = $service->id;
			$val = $status == "Activo" ? "checked" :  "";
			$switch = '<center><div class="custom-control custom-switch">
			<input type="checkbox" class="custom-control-input" id="customSwitch'.$id.'" onclick="changeStatus('.$id.','."'".$status."'".')" '.$val.'>
			<label class="custom-control-label" for="customSwitch'.$id.'"></label>		
			</div></center>';
			return $switch;
		})
		// ->addColumn('department_name', function(Service $service){
		// 	return  $service->department->name;
		// })
		->rawColumns(['status', 'id', 'name'])
		->toJson();
	}

	public function updateStatus(Request $data)
	{
		$status = ($data['status'] == "Activo") ? "Inactivo" : (($data['status'] == "Inactivo") ? "Activo" : '');
		$user = Service::find($data['id']); 
		$user->update(['status' => $status]);
	}

	function listSubjects($service_id){
		$datos = array();
		foreach (Subject::findById($service_id) as $subject):
			$row_array['id']  = $subject->id;
			$row_array['nombre']  = $subject->name;
			array_push($datos, $row_array);
		endforeach;     
		echo json_encode($datos, JSON_FORCE_OBJECT);
	}

	function listUsers(Service $service){
		$users=$service->users;
		$datos = array();
		foreach ($users as $user):
			$row_array['id']  = $user->id;
			$row_array['name']  = $user->name;
			$row_array['email']  = $user->email;
			$row_array['picture']  = $user->picture;

			array_push($datos, $row_array);
		endforeach;     
		echo json_encode($datos, JSON_FORCE_OBJECT);
	}

	function getUsers(Service $service){
		$users=auth()->user()->department->users;
		$datos = array();
		foreach ($users as $user):
			$row_array['id']  = $user->id;
			$row_array['name']  = $user->name;
			$row_array['email']  = $user->email;
			$row_array['picture']  = $user->picture;
			array_push($datos, $row_array);
		endforeach;     
		echo json_encode($datos, JSON_FORCE_OBJECT);
	}


	public function addSubject(){
		$data = request()->validate([
			'subject' => 'required',
			'service_id' => 'required',
		],[
			'subject.required' => 'Debe ingresar un asunto',
		]);
		
		$subject=Subject::create([
			'name' => $data['subject'],
			'service_id' => $data['service_id'],
		]);

		return back();
	}

	public function destroySubject(Subject $subject){
		$subject->delete();
		return back();
	}

	public function attachUser(){
		$data = request()->validate([
			'user_id' => 'required',
			'service_id' => 'required',
		],[
			'user_id' => 'Debe seleccionar un usuario',
		]);

		$service=Service::findOrFail($data['service_id']);

		$service->users()->attach($data['user_id']);
		return back();

	}

	public function detachUser(Request $data){
		
		$service=Service::findOrFail($data['service_id']);

		$service->users()->detach($data['user_id']);
		return back();
	}

}
