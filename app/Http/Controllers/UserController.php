<?php

namespace App\Http\Controllers;

use App\User;
use App\Ticket;
use App\Department;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use Hash;
use DataTables;
use DB;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth'); 
	}

	function index()
	{
		$users=User::all();
		return view('users.index', compact('users'));
	}

	function create()
	{
		return view('users.create');
	}

	function show(User $user)
	{
		return view('users.show', compact('users'));
	}

	public function profile()
	{
		$findStatus = Ticket::findStatusCount();
		$departments=Department::all(); 
		$profile=true;
		return view('profile.index', compact('departments', 'profile', 'findStatus'));
	}

	public function updateProfile()
	{
		$data=request()->validate([
			'name' => 'required',
			'email' => ['required', 'email', Rule::unique('users')->ignore(auth()->user()->id)],
			'department_id' => 'required',
			
		], [
			'name.required' => 'Debe ingresar su nombre',
			'email.required' => 'Debe ingresar un correo electrónico',
			'email.email' => 'El correo electrónico no es válido',
			'email.unique' => 'Este correo ya está registrado',
			'department_id.required' => 'Debe seleccionar el departamento al que pertenece',
		]);
		auth()->user()->update($data);
	}

	public function updatePassword()
	{
		$data=request()->validate([
			'password_old' => 'required',
			'password' => 'required|confirmed|min:6',
		],[
			'password_old.required' => 'El campo contraseña es obligatorio',
			'password.required' => 'El campo nueva contraseña es obligatorio',
			'password.required' => 'El campo nueva contraseña es obligatorio',
			'password.confirmed' => 'Las contraseñas no coinciden',
		]);

		if (Hash::check($data['password_old'], auth()->user()->password)) {
			auth()->user()->update(['password' => bcrypt($data['password'])]);
			return redirect('profile')->with('success', 'creado');
		}else{
			return redirect()->route('profile');
		}
	}

	public function updateImage(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'image' => 'required|image|mimes:jpg,jpeg,gif,png|max:2048'
		],[
			'image.required' => 'El campo imagen es obligatorio',
			'image.image' => 'El formato es incorrecto',
			'image.mimes' => ' La imagen debe ser un archivo de tipo: jpg, jpeg, gif, png.',
			'image.max' => 'El tamaño máximo de la imagen es de 2 MB',
		]);

		if ($validator->fails()){
			return response()->json(['is' => 'failed', 'error' => $validator->getMessageBag()]);
		}else{
			$image = $request['image'];
			$new_name = rand().'.'.$image->getClientOriginalName();
			$image->move(public_path('images/profiles/'), $new_name);
			auth()->user()->update(['picture' => '/images/profiles/'.$new_name]);
			return response()->json(['is'=>'success']);
		}		
	}

	public function getUser(User $user)
	{
		echo json_encode($user, JSON_FORCE_OBJECT);
	}

	public function getUsers($filter)
	{
		$users = User::findByStatus($filter);
		return Datatables::of($users)
		->editColumn('id', '<b>{{ $id }}</b>')
		->editColumn('is_admin', function(User $user){
			$is_admin = ($user->is_admin == true) ? 1 : 0;
			$id = $user->id;
			$val = $is_admin == "1" ? "checked" :  "";
			$switch = '<center><div class="custom-control custom-switch">
			<input type="checkbox" class="custom-control-input" id="customSwitch'.$id.'" onclick="changeTypeUser('.$id.','."'".$is_admin."'".')" '.$val.'>
			<label class="custom-control-label" for="customSwitch'.$id.'"></label>		
			</div></center>';
			return $switch;
		})
		->rawColumns(['id', 'is_admin'])
		->toJson();
	}

	public function updateType(Request $data)
	{
		$type = ($data['type'] == 1) ? 0 : (($data['type'] == 0) ? 1 : '');
		$user = User::find($data['id']); 
		$user->update(['is_admin' => $type]);
	}

}
