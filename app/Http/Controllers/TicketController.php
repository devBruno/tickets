<?php

namespace App\Http\Controllers;

use DB;
use DataTables;
use Carbon\Carbon;
use Validator;
use App\Ticket;
use App\Department;
use App\User;
use App\Service;
use App\Message;
use App\File;
use App\Subject;
use Illuminate\Http\Request;
use App\Notifications\MessageSend;
use App\Notifications\TicketClosed;

use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\EloquentDataTable;
use Illuminate\Http\UploadedFile;
use Illuminate\Database\Query\Builder;

/*use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;
use Yajra\DataTables\Html\Builder; */

class TicketController extends Controller
{

	public function __construct()
	{
		$this->middleware('auth'); 
	}

	function index()
	{
		$tickets = auth()->user()->tickets_all;
		return view('tickets.index', compact('tickets'));
	}

	function create()
	{
		// $departments = Department::all();
		$departments = Department::where('name', 'Sistemas')->orWhere('name', 'Conservación')->get();
		// $services_dep = Service::where('department_id', $departments->id)->get();
		$abrir_ticket = true;
		return view('tickets.create', compact('departments', 'abrir_ticket', 'services_dep'));
	}

	function create_admin()
	{
		$users_in_open_ticket = User::all();
		$departments = Department::where('name', auth()->user()->department->name)->first();
		$services_dep = Service::where('department_id', $departments->id)->get();
		$abrir_ticket = true;
		return view('tickets.create_admin', compact('departments', 'abrir_ticket', 'services_dep', 'users_in_open_ticket'));
	}

	function created(Ticket $ticket)
	{
		return redirect("tickets/nuevo")->with('success', 'Ticket creado #' . $ticket->id)->with('idTicket', $ticket->id);
	}

	function created_admin(Ticket $ticket)
	{
		return redirect("tickets/nuevo/admin")->with('success', 'Ticket creado #' . $ticket->id)->with('idTicket', $ticket->id);
	}

	function show(Ticket $ticket)
	{
		$tickets=true;
		Carbon::setLocale('es');
		return view('tickets.show', compact('ticket','tickets'));
	}

	function show_admin(Ticket $ticket)
	{
		$user_tickets = true;
		Carbon::setLocale('es');
		return view('tickets.show', compact('ticket', 'user_tickets'));
	}

	public function store()
	{
		$data = request()->validate([
			'user_id' => 'required',
			'email' => '',
			'message' => 'required',
			'subject' => 'required',
			'department_id' => 'required',
			'service_id' => 'required',	
			'file' => '',
		],[
			'user_id.required' => 'Se debe asignar un responsable al ticket',
			'message.required' => 'El campo mensaje es obligatorio.',
			'department_id.required' => 'El campo departamento es obligatorio.',
			'subject.required' => 'El campo asunto es obligatorio.',
			'service_id.required' => 'El campo servicio es obligatorio.',
		]);

		$ticket=Ticket::create([
			'subject' => $data['subject'],
			'service_id' => $data['service_id'],
			'user_id' => $data['user_id'],
			'department_id' => $data['department_id']
		]);

		$message=Message::create([
			'message' => $data['message'],
			'ticket_id' => $ticket->id,
			'user_id' 	=> auth()->user()->id,
		]);

		if(auth()->user()->id==$data['user_id'])
				$this->createMessageResponse($ticket);

		$countSubject = Subject::where('name', $data['subject'])->count();
		if ($countSubject == 0) {
			Subject::create([
				'name' => $data['subject'],
				'service_id' => $data['service_id']
			]);
		}
		
		if (isset($data['file'])) {
			foreach ($data['file'] as $file):
				$name = $file->getClientOriginalName();
				$type = $file->getClientOriginalExtension();
				$route = time().$name;
				$file->move(public_path().'/files', $route);

				File::create([
					'name' => $name,
					'type' => $type,
					'route' => 'files/'.$route,
					'message_id' => $message->id
				]);
			endforeach;
		}
		echo $ticket->id;
	}

	public function createMessageResponse($ticket)
	{
		$mensaje = '
		<p>Muchas gracias por contactar al el soporte técnico de AMMMEC S.A. DE C.V. 
		En un lapso de 12 horas uno de nuestros ingenieros lo atenderá para solventar su pectición. </p>
		<p>Su satisfacción es muy importante para nosotros. </p>
		<p>
		<b>AMMMEC S.A de C.V</b><br>
		Departamento de ' . $ticket->service->department->name . '<br>
		<b class="text-danger">t:</b> +52 (493) 878 8552<br>
		<b class="text-danger">c:</b> ' . $ticket->service->department->phone . '<br>
		<b class="text-danger">e:</b> ' . $ticket->service->department->email . '<br>
		<b class="text-danger">w:</b><a href="https://www.ammmec.com"> https://www.ammmec.com</a>  
		<b class="text-danger">w:</b><a href="https://www.dingo.com"> https://www.dingo.com</a> 
		<b class="text-danger">w:</b><a href="https://www.paulfans.com"> https://www.paulfans.com</a>
		<b class="text-danger">w:</b><a href="www.c-sert.com"> www.c-sert.com</a>';

		Message::create([
			'message' => $mensaje,
			'ticket_id' => $ticket->id,
			'user_id' 	=> 1,
		]);
	}

	public function newMessage(Ticket $ticket)
	{
		$data = request()->validate([
			'message' => 'required',
			'file' 	  => '',
		],[
			'message.required' => 'No has introducido ningún mensaje.'
		]);

		$message=Message::create([
			'message' 	=> $data['message'],
			'ticket_id' => $ticket->id,
			'user_id' 	=> auth()->user()->id,
		]);

		//Se manda la notificación de parte del que creo el ticket
		if($ticket->user_id==auth()->user()->id){
			$ticket->status = "Abierto";
			$ticket->status_admin = "Contestado";
			foreach($ticket->users as $user):
				$user->notify(new MessageSend($message));
			endforeach;
		}else{
			$ticket->status = "Contestado";
			$ticket->status_admin = "Abierto";
			$ticket->user->notify(new MessageSend($message));
		}

		$ticket->update();

		if (isset($data['file'])) {
			foreach ($data['file'] as $file):
				$name = $file->getClientOriginalName();
				$type = $file->getClientOriginalExtension();
				$route = time().$name;
				$file->move(public_path().'/files', $route);

				File::create([
					'name' => $name,
					'type' => $type,
					'route' => 'files/'.$route,
					'message_id' => $message->id
				]);
			endforeach;
		}
	}

	function listServices($departmentId){	
		$datos = array();
		foreach (Service::findById($departmentId)->where('status', 'Activo') as $service){
			$row_array['ids']  = $service->id;
			$row_array['nombre']  = $service->name;
			$row_array['descripcion'] = $service->description;
			array_push($datos, $row_array);
		}    
		echo json_encode($datos, JSON_FORCE_OBJECT);
	}

	public function closeTicket(Ticket $ticket)
	{
		$ticket->status="Cerrado";
		$ticket->status_admin="Cerrado";
		$ticket->update();

		if($ticket->user_id==auth()->user()->id){
			foreach($ticket->users as $user):
				$user->notify(new TicketClosed($ticket));
			endforeach;
		}else{
			$ticket->user->notify(new TicketClosed($ticket));
		}
		return back();	
	}

	function indexAdmin()
	{
		$user_tickets = auth()->user()->tickets;
		return view('tickets.index_admin', compact('user_tickets'));
	}

	function getTickets($status)
	{	
		$tickets = ($status!='All') ? Ticket::findByStatus($status) : auth()->user()->tickets_all;
		return $this->dataTable($tickets);
	}

	function getTickets_admin($status)
	{	
		$tickets = ($status!='All') ? auth()->user()->tickets->where('status_admin',$status) : auth()->user()->tickets;
		return $this->dataTable($tickets);
	}

	function dataTable($tickets)
	{
		return Datatables::of($tickets)
		->setRowId(function($ticket){
			return $ticket->id;
		})
		->setRowData(['data-name' => 'row-{{ $subject }}',])
		->addColumn('user_name', function(Ticket $ticket){
			return  $ticket->user->name;
		})
		->editColumn('updated_at', function(Ticket $ticket){
			Carbon::setLocale('es');
			return "<center>".$ticket->updated_at->diffForHumans()."<center>";
		})
		->editColumn('status', function(Ticket $ticket){
			$status = $ticket->status;
			$val = $status == "Abierto" ? "success" : ($status == "Cerrado" ? "secondary" : "danger");
			$span = "<span class='badge badge-$val d-block'>$status</span>";
			return $span;
		})	
		->editColumn('status_admin', function(Ticket $ticket){
			$status = $ticket->status_admin;
			$val = $status == "Abierto" ? "success" : ($status == "Cerrado" ? "secondary" : "danger");
			$span = "<span class='badge badge-$val d-block'>$status</span>";
			return $span;
		})	
		->editColumn('id', '<b>{{ $id }}</b>')
		->rawColumns(['status', 'id', 'status_admin','updated_at', 'user_name'])
		->toJson();
	}


	public function download(File $file)
	{
		$pathtoFile = public_path().'/'.$file->route;
		return response()->download($pathtoFile);
	}

}



































