<?php

namespace App\Http\Controllers;

use App\Department;
use Illuminate\Http\Request;

class DepartmentController extends Controller
{

	public function __construct()
	{
		$this->middleware('auth'); 
	}
	
	function index()
	{
		$department = Department::all();
		return $department;

	}
}
