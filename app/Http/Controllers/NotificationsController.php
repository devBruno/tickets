<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Notifications\DatabaseNotification;


class NotificationsController extends Controller
{
    public function __construct()
	{
		$this->middleware('auth');
	}

	public function index()
	{
		return view('notifications.index');
	}

	public function read($notification_id, $route, $id)
	{
		DatabaseNotification::find($notification_id)->markAsRead();
		return redirect()->route($route, $id);
	}

	public function destroy($id)
	{
		DatabaseNotification::find($id)->delete();
		return back()->with('flash', 'Notificacion eliminada');
	}
}
