<?php

namespace App;

use DB;
use App\Traits\DatesTranslator;
use App\Events\TicketCreated;
use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
	use DatesTranslator;

	protected $fillable = [
		'subject', 'status', 'service_id', 'user_id', 'department_id'
	];

	protected $guarded = [];

	protected $dispatchesEvents = [
		'created' => TicketCreated::class
	];

	public function messages(){
		return $this->hasMany(Message::class)->orderby('id','DESC');
	} 

	public function files(){
		return $this->hasMany(File::class);
	} 

	public function user(){
		return $this->belongsTo(User::class);
	}

	public function users(){
		return $this->belongsToMany(User::class);
	}

	public function service(){
		return $this->belongsTo(Service::class);
	}

	public function department(){
		return $this->belongsTo(Department::class);
	}

	static public function findById()
	{
		return static::select('id')->orderby('created_at','DESC')->first();
	}

	static public function findByStatus($status)
	{
		return static::where('status','like', '%' . $status . '%')
		->where('user_id','=',auth()->user()->id)
		->orderby('id','DESC')->get();
	}

	public static function findStatusCount()
	{
		return DB::table('tickets')->select('status', DB::raw('count(*) as status_count'))->groupBy('status')->where('department_id', '=', auth()->user()->department->id)->get();
	}
	
}
