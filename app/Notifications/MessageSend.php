<?php

namespace App\Notifications;

use App\Message;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class MessageSend extends Notification
{
	use Queueable;
	protected $Message;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Message $message)
    {
    	$this->message = $message;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
    	return ['database', 'mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        if($this->message->user->id == $this->message->ticket->user->id)
            $url = '/tickets/' . $this->message->ticket->id; 
        else
            $url = '/tickets/' . $this->message->ticket->id . '/admin'; 

        $not_name = explode(" " , $notifiable->name);

    	return (new MailMessage)
        ->from($this->message->user->email)
        ->line($this->message->user->name . ' ha respondido un mensaje en el ticket #' . $this->message->ticket->id)
    	->subject($this->message->user->name . ' ha respondido un mensaje')
    	->greeting($not_name[0] . ',')
    	->action('Ver mensaje', url($url))
    	->salutation('ERP-AMMMEC Sistema de tickets');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
         if($this->message->ticket->user_id==auth()->user()->id)
            $route = 'tickets.show';
        else
            $route = 'tickets.show_admin';

    	return [
    		'text' => "<b>" . auth()->user()->name . "</b> ha respondido un mensaje en el ticket #" . $this->message->ticket->id,

    		'icon' => '<i class="fas fa-envelope-square"></i>',
    		'picture' => auth()->user()->picture,
    		'route_name' => $route,
    		'id' => $this->message->ticket->id
    	];
    }
}
