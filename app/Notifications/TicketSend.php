<?php

namespace App\Notifications;

use App\Ticket;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class TicketSend extends Notification
{
    use Queueable;
    protected $ticket;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Ticket $ticket)
    {
        $this->ticket = $ticket;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database','mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    
    public function toMail($notifiable)
    {
        $not_name = explode(" " , $notifiable->name);

        return (new MailMessage)
        ->from($this->ticket->user->email)
        ->subject('Nuevo ticket de ' . $this->ticket->user->name)
        ->greeting('Hola ' . $not_name[0] . ',')
        ->line('Tienes un nuevo ticket de ' . $this->ticket->user->name . ':')
        ->line('"' . $this->ticket->subject . '"')
        ->action('Ver ticket', route('tickets.show_admin',$this->ticket->id))
        ->salutation('ERP-AMMMEC Sistema de tickets');
    }

    // public function toMail($notifiable)
    // {
    //     return (new MailMessage)
    //     ->from($this->ticket->user->email)
    //     ->greeting($notifiable->name . ',')
    //     ->subject('Nuevo ticket de ' . $this->ticket->user->name)
    //     ->line($this->ticket->subject)
    //     ->action('Ver ticket', route('tickets.show_admin',$this->ticket->id))
    //     ->salutation('ERP-AMMMEC Sistema de tickets')
    //     ->line('Gracias por utilizar el soporte técnico de Ammmec S.A. de C.V.');
    // }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'text' => "<b>" . $this->ticket->user->name. "</b> ha creado un nuevo ticket para el departamento de " . $this->ticket->department->name,
            'icon' => "<i class='far fa-sticky-note'></i>",
            'picture' => $this->ticket->user->picture,
            'route_name' => 'tickets.show_admin',
            'id' => $this->ticket->id
        ];
    }
}
