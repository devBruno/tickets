<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
	protected $fillable = [
		'name' 
	];

	public function services(){
		return $this->hasMany(Service::class);
	} 

	public function users(){
		return $this->hasMany(User::class);
	} 

	public function admin_users(){
		return $this->hasMany(User::class)->where('is_admin','1');
	} 

	public function tickets(){
		return $this->hasMany(Ticket::class);
	}
}
