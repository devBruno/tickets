<?php

namespace App\Listeners;

use App\User;
use App\Events\TicketCreated;
use App\Notifications\TicketSend;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Notification;


class NotifyDepartmentAboutNewTicket
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  TicketCreated  $event
     * @return void
     */
    public function handle(TicketCreated $event)
    {
        $users = $event->ticket->service->users;

        if($users->isNotEmpty()){

           foreach($users as $user):
            $event->ticket->users()->attach($user->id);
            endforeach;

        }else{

            $users = $event->ticket->department->admin_users;

            foreach($users as $user):
                if($user->is_admin=='1')
                     $event->ticket->users()->attach($user->id);
            endforeach;

        }

        Notification::send($users, new TicketSend($event->ticket));
    }
}
