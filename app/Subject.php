<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Subject extends Model
{
	protected $fillable = [
		'name', 'service_id', 
	];

	public function service(){
		return $this->belongsTo(Service::class);
	} 

	public static function findById($service_id){
		return static::where(compact('service_id'))->get();
	} 
}
