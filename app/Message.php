<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $fillable = [
		'message', 'ticket_id', 'user_id'
	];

    public function ticket(){
		return $this->belongsTo(Ticket::class);
	}

	public function user(){
		return $this->belongsTo(User::class);
	}

	public function files()
	{
		return $this->hasMany(File::class);
	}
}
